package reagents.data.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

import reagents.app.ItemTemplate;
import reagents.app.ServiceTemplate;
import reagents.app.order.CatalogService;

@Entity
@DynamicUpdate
@Table(name="orders_catalog")
@Proxy(lazy=false)
public class Catalog extends ItemTemplate implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	public int id;
	
	@Column(name="type" , length=64)
	private String type;
	
	@Column(name="code", length=64)
	private String code;
	
	@Column(name="cost")
	private int cost;
	
	@Column(name="contain")
	private int contain;
	
	@Column(name="note" , length=64)
	private String note;

	@Column(name="editedDate")
	private Date editedDate;

	@Column(name="editedBy", length=64)
	private String editedBy;


	public Catalog(){
	}

	public Catalog(String type, int contain, String note, String code, int cost) {
		super();
		this.type = type;
		this.code = code;
		this.cost = cost;
		this.contain = contain;
		this.note = note;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getContain() {
		return contain;
	}

	public void setContain(int contain) {
		this.contain = contain;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	public ServiceTemplate getService(){
		return (new CatalogService());
	}

	public void lock(String userName){
		editedBy = userName;
		editedDate = new Date();
	}

	public void unlock(){
		editedBy = "";
		editedDate = null;
	}

	public boolean isLocked(String userName) {
		return !(editedBy == null || editedBy.equals("") || editedBy.equals(userName))
				&& editedDate != null && editedDate.getTime() + 900000 <= System.currentTimeMillis();
	}
}
