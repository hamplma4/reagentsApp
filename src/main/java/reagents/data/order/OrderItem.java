package reagents.data.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

import reagents.app.ItemTemplate;
import reagents.app.ServiceTemplate;
import reagents.app.order.OrderItemService;


@Entity
@DynamicUpdate
@Table(name="orders_item")
@Proxy(lazy=false)
public class OrderItem extends ItemTemplate implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	public int id;
	
	@Column(name="idOrder")
	private int idOrder;
	
	@Column(name="type" , length=64)
	private String type;
	
	@Column(name="code", length=64)
	private String code;
	
	@Column(name="cost")
	private int cost;
	
	@Column(name="contain")
	private int contain;
	
	@Column(name="count")
	private int count;
	
	@Column(name="note", length=63)
	private String note;
	
	private OrderItem backUp;
	
	public OrderItem(){
		
	}
	
	
	public OrderItem(Catalog c) {
		this.code = c.getCode();
		this.cost = c.getCost();
		this.count = 1;
		this.type = c.getType();
		this.contain = c.getContain();
	}

	public OrderItem(OrderItem orderItem) {
		this.code = orderItem.getCode();
		this.cost = orderItem.getCost();
		this.count = orderItem.getCount();
		this.type = orderItem.getType();
		this.contain = orderItem.getContain();
		this.note = orderItem.getNote();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public String getType() {
		return type;
	}

	public String getTypeMinLength(){
		return "5";
	}

	public String getTypeMaxLength(){
		return "32";
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public String getCodeMaxLength(){
		return "32";
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getCount() {
		return count;
	}

	public String getCountMinValue(){
		return "0";
	}

	public String getCountMaxValue(){
		return "20";
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public int getContain() {
		return contain;
	}

	public String getContainMinValue(){
		return "1";
	}

	public String getContainMaxValue(){
		return "100";
	}

	public void setContain(int contain) {
		this.contain = contain;
	}

	public String getNote() {
		return note;
	}

	public String getNoteMaxLength(){
		return "64";
	}

	public void setNote(String note) {
		this.note = note;
	}

	public OrderItem getBackUp() {
		return backUp;
	}
	
	public void createBackUp(){
		backUp = new OrderItem(this);
	}

	public ServiceTemplate getService(){
		return new OrderItemService();
	}



}
