package reagents.data.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

import reagents.app.ItemTemplate;
import reagents.app.ServiceTemplate;
import reagents.app.order.OrderService;


@Entity
@DynamicUpdate
@Table(name="orders")
@Proxy(lazy=false)
public class Order extends ItemTemplate implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	public int id;
	
	@Column(name="label" , length=63)
	private String label;
	
	@Column(name="count")
	private int count;
	
	@Column(name="costSum")
	private int sumCost;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="acceptedDate")
	private Date acceptedDate;

	@Column(name="manufacturer", length=63)
	private String manufacturer;
	
	@Column(name="editedBy")
	private String editedBy;
	
	@Column(name="editedDate")
	private Date editedDate;
	
	public Order(){	
	}
	
	public Order(int id, String label, Date date, int count, int sumCost) {
		super();
		this.id = id;
		this.label = label;
		this.date = date;
		this.count = count;
		this.sumCost = sumCost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSumCost() {
		return sumCost;
	}

	public void setSumCost(int sumCost) {
		this.sumCost = sumCost;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getAcceptedDate() {
		return acceptedDate;
	}

	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}

	public void setEditedDate(Date editedDate) {
		this.editedDate = editedDate;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public void lock(String user) {
		editedBy = user;
		editedDate = new Date();
	}

	public void unlock(){
		editedBy = "";
		editedDate = null;
	}

	public boolean isLocked(String userName){
		if (editedBy == null || editedBy.equals("") || editedBy.equals(userName) ) return false;
		if (editedDate == null) return false;
		if (editedDate.getTime() + 900000 < System.currentTimeMillis()) return false;
		return true;
	}

	public ServiceTemplate getService(){
		return new OrderService();
	}

}
