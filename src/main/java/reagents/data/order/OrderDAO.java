package reagents.data.order;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import reagents.app.hibernate.HibernateUtil;

public class OrderDAO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings("rawtypes")
	public List getAll() throws HibernateException {
		return 	HibernateUtil
				.getSessionFactory()
				.openSession()
				.createCriteria(Order.class).list();
	} 
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List reload(List selected) throws HibernateException {
		Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Order.class);
		Disjunction disjunction = Restrictions.disjunction();
		System.out.println("DAO reload " + selected);
		if(selected != null){
			for(Order o: (List<Order>)selected){
					System.out.println("pridavam"+o.getId());
					disjunction.add(Restrictions.eq("id", o.getId()));
			}
		}	
		criteria.add(disjunction);
		return criteria.list();
	}
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void lock(List forLock, String user) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Order o: (List<Order>) forLock){
			o.lock(user);
			session.update(o);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void update(List forUpdate) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Order o: (List<Order>) forUpdate){
			o.unlock();
			session.update(o);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void save(List forSave) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for(Order o:(List<Order>) forSave){
			o.unlock();
			session.save(o);
		}
		tx.commit();	
	}
	/* ----------------------------------------------------------------------- */
	public int getNextId(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session
			    .createCriteria(Order.class)
			    .setProjection(Projections.max("id"));
		Integer maxId = (Integer)criteria.uniqueResult();
		if(maxId==null) return 0;
		return (maxId+1);
	}
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings("rawtypes")
	public List getNonAceppted() {
		return HibernateUtil.getSessionFactory().openSession()
				.createCriteria(Order.class)
				.add(Restrictions.eq("accepted", false))
				.list();
	}
	/* ----------------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void unlock(String userName) {
		List<Order> list = HibernateUtil
				.getSessionFactory()
				.openSession()
				.createCriteria(Order.class)
				.add(Restrictions.eq("editedBy", userName))
				.list();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for(Order o: list){
			o.unlock();
			session.update(o);
		}
		tx.commit();
	}
}
