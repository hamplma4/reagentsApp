package reagents.data.order;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import reagents.app.hibernate.HibernateUtil;

public class CatalogDAO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getAll() throws HibernateException {
		return HibernateUtil.getSessionFactory().openSession().createCriteria(Catalog.class).list();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List reload(List selected) throws HibernateException {
		Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Catalog.class);
		Disjunction disjunction = Restrictions.disjunction();
		for(Catalog c: (List<Catalog>)selected){
				disjunction.add(Restrictions.eq("id", c.getId()));
		}
		criteria.add(disjunction);
		return criteria.list();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void update(List forUpdate) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Catalog c: (List<Catalog>)forUpdate){
			session.update(c);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void save(List forSave) throws HibernateException {
		int id = getNextId();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for (Catalog c: (List<Catalog>) forSave){
			c.setId(id);
			session.save(c);
			id++;
		}
		tx.commit();	
	}
	/* ---------------------------------------------------------------- */
	private int getNextId() throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session
			    .createCriteria(Catalog.class)
			    .setProjection(Projections.max("id"));
		Integer maxId = (Integer)criteria.uniqueResult();
		if(maxId==null) return 0;
		return maxId+1;
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public void deleteAll() {
		List<Catalog> list = (List<Catalog>)this.getAll();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for (Catalog c: list){
			session.delete(c);
		}
		tx.commit();	
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({"rawtypes","unchecked"})
	public void lock(List forLock, String user) throws HibernateException {
		for(Catalog c: (List<Catalog>)forLock){
			c.lock(user);
		}
		this.update(forLock);
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({"rawtypes","unchecked"})
	public void unlock(String userName) throws HibernateException {
		List<Catalog> list = HibernateUtil.getSessionFactory().openSession()
				.createCriteria(Catalog.class)
				.add(Restrictions.eq("editedBy",userName))
				.list();
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Catalog c: list){
			c.unlock();
			session.update(c);
		}
		session.getTransaction().commit();
		session.close();
	}	
	/* ---------------------------------------------------------------- */

}
