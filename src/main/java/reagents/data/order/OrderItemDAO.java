package reagents.data.order;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import reagents.app.hibernate.HibernateUtil;

public class OrderItemDAO {

	@SuppressWarnings("rawtypes")
	public List getAll() throws HibernateException {
		return HibernateUtil.getSessionFactory().openSession()
					.createCriteria(OrderItem.class).list();
	}
	/* ---------------------------------------------------------------- */	
	@SuppressWarnings("rawtypes")
	public List reload(List selected) throws HibernateException {
		/* This method is not use*/
		return null;
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void update(List forUpdate) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(OrderItem i: (List<OrderItem>)forUpdate){
			session.update(i);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void save(List forSave) throws HibernateException {
		int id = getNextId();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for (OrderItem o: (List<OrderItem>)forSave){
			o.setId(id);
			session.save(o);
			id++;
		}
		tx.commit();	
	}
	/* ---------------------------------------------------------------- */
	private int getNextId() throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session
			    .createCriteria(OrderItem.class)
			    .setProjection(Projections.max("id"));
		Integer maxId = (Integer)criteria.uniqueResult();
		if(maxId==null) return 0;
		return maxId+1;
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings("rawtypes")
	public List getByOrder(Order o) {	 
        return HibernateUtil.getSessionFactory()
        					.openSession()
        					.createCriteria(OrderItem.class)
        					.add(Restrictions.eq("idOrder", o.getId()))
        					.list();	
	}	
	/* ---------------------------------------------------------------- */
	@SuppressWarnings("rawtypes")
	public void lock(List forLock, String user) throws HibernateException {
		/* This method is not use*/
		
	}
	@SuppressWarnings("rawtypes")
	public List getNonConsumed() throws HibernateException {
		/* This method is not use*/
		return null;
	}
	@SuppressWarnings("rawtypes")
	public void unlock(List forUnlock) throws HibernateException {
		/* This method is not use*/
		
	}
	

}
