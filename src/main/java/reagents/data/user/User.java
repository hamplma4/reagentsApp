package reagents.data.user;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.HibernateException;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;
import reagents.app.ItemTemplate;
import reagents.app.ServiceTemplate;
import reagents.app.user.UserService;


@Entity
@DynamicUpdate
@Table(name="users")
@Proxy(lazy=false)
public class User extends ItemTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true)
	private int id;

	@Column(name="userName", length=64, unique=true, nullable=false)
	private String userName;
	
	@Column(name="email", length=64, unique=true, nullable=false)
	private String email;
	
	@Column(name="firstName", length=64, nullable=false)
	private String firstName;
	
	@Column(name="lastName", length=64, nullable=false)
	private String lastName;
	
	@Column(name="passwd", length=255, nullable=false)
	private String passwd;
	
	@Column(name="telephone", length=20)
	private String telephone;
	
	@Column(name="degreesBefore", length=32)
	private String degreesBefore;
	
	@Column(name="degreesAfter", length=32)
	private String degreesAfter;
	
	@Column(name="role")
	private int role;

	@Column(name="editedBy", length=64)
	private String editedBy;

	@Column(name="editedDate")
	private Date editedDate;

	@Transient
	private User backUp;

	public User(){
	}

	public User(User u) {
		this.userName = u.userName;
		this.email = u.email;
		this.firstName = u.firstName;
		this.lastName = u.lastName;
		this.passwd = u.passwd;
		this.telephone = u.telephone;
		this.degreesBefore = u.degreesBefore;
		this.degreesAfter = u.degreesAfter;
		this.role = u.role;
		this.editedBy = u.editedBy;
		this.editedDate = u.editedDate;
		this.backUp = null;
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getDegreesBefore() {
		return degreesBefore;
	}

	public void setDegreesBefore(String degreesBefore) {
		this.degreesBefore = degreesBefore;
	}

	public String getDegreesAfter() {
		return degreesAfter;
	}

	public void setDegreesAfter(String degreesAfter) {
		this.degreesAfter = degreesAfter;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public User getBackUp() {
		return backUp;
	}

	public String getName(){
		return 	degreesBefore+" "+
				firstName+" "+lastName+" "+
				degreesAfter;
	}

	public boolean isLocked(String userName){
		if (editedBy == null || editedBy.equals("") || editedBy.equals(userName) ) return false;
		if (editedDate == null) return false;
		if (editedDate.getTime() + 900000 < System.currentTimeMillis()) return false;
		return true;
	}

	@Override
	public ServiceTemplate getService(){
		return new UserService();
	}

	public void reload() throws HibernateException{
		User u = (new UserDAO()).getByUserName(userName);
		this.userName = u.userName;
		this.email = u.email;
		this.firstName = u.firstName;
		this.lastName = u.lastName;
		this.passwd = u.passwd;
		this.telephone = u.telephone;
		this.degreesBefore = u.degreesBefore;
		this.degreesAfter = u.degreesAfter;
		this.role = u.role;
		this.editedBy = u.editedBy;
		this.editedDate = u.editedDate;
	}

	public void lock(String userName){
		editedDate = new Date();
		editedBy = userName;
		UserService.update(this);
	}

	public void unlock(){
		editedDate = null;
		editedBy = "";
		UserService.update(this);
	}

	public void save(){
		editedDate = null;
		editedBy = "";
		UserService.save(this);
	}

	public void update(){
		editedDate = null;
		editedBy = "";
		UserService.update(this);
	}

	public void createBackUp(){
		backUp = new User(this);
	}

	
}
