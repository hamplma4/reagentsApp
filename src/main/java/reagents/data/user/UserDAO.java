package reagents.data.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import reagents.app.exception.DbIOException;
import reagents.app.hibernate.HibernateUtil;
import reagents.data.kit.Kit;


public class UserDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List getAll() throws DbIOException{
		List<User> users = new ArrayList<>();
		try {
			users = (List<User>) HibernateUtil.getSessionFactory().openSession().createCriteria(User.class).list();
		}catch (HibernateException ex){
			throw new DbIOException(ex);
		}
		return users;
	}
	/* --------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public User getByUserName(String userName) {
		List<User> users = (List<User>)HibernateUtil	
						.getSessionFactory()
						.openSession()
						.createCriteria(User.class)
						.add(Restrictions.eq("userName", userName).ignoreCase())
						.list();
		if(users.isEmpty()){
			return null;
		} 
		else {
			return users.get(0);
		}
	}
	/* --------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public User getByMail(String mail) {
		List<User> users = (List<User>)HibernateUtil
				.getSessionFactory()
				.openSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("email", mail).ignoreCase())
				.list();
		if(users.isEmpty()){
			return null;
		}
		else {
			return users.get(0);
		}
	}
	/* --------------------------------------------------------------- */
	public void save(User u) throws HibernateException {
		u.setId(this.getNextId());
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(u);
		session.getTransaction().commit();
		session.close();
	}
	/* --------------------------------------------------------------- */
	public void update(User u) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(u);
		session.getTransaction().commit();
		session.close();
	}
	/* --------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void lock(User u, String userName) throws HibernateException {
		u.lock(userName);
		this.update(u);
	}
	/* --------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void unlock(String userName) throws HibernateException {
		List<User> list = HibernateUtil.getSessionFactory().openSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("editedBy",userName))
				.list();
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(User u: list){
			u.unlock();
			session.saveOrUpdate(u);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* --------------------------------------------------------------- */
	private int getNextId() throws HibernateException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session
				.createCriteria(Kit.class)
				.setProjection(Projections.max("id"));
		Integer maxId = (Integer)criteria.uniqueResult();
		return maxId+1;
	}
}
