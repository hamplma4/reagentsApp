package reagents.data.warnings.kits;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import reagents.app.hibernate.HibernateUtil;

public class WarnKitsSettDAOImpl implements WarnKitsSettDAO{

	@Override
	public WarnKitsSett getByType(String type) throws HibernateException{
		@SuppressWarnings("unchecked")
		List<WarnKitsSett> warnings = 
		HibernateUtil	.getSessionFactory()
						.openSession()
						.createCriteria(WarnKitsSett.class)
						.add(Restrictions.eq("type", type))
						.list(); 
		if(warnings.size()==0){
			return this.getByType("default");
		}
		else{
			return warnings.get(0);
		}
	}

}
