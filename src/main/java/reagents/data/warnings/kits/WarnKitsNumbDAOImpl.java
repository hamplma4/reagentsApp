package reagents.data.warnings.kits;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import reagents.app.hibernate.HibernateUtil;

public class WarnKitsNumbDAOImpl implements WarnKitsNumbDAO {
	
	@Override
	public WarnKitsNumb getByType(String type) throws HibernateException{
		@SuppressWarnings("unchecked")
		List<WarnKitsNumb> warnings = 
		HibernateUtil	.getSessionFactory()
						.openSession()
						.createCriteria(WarnKitsNumb.class)
						.add(Restrictions.eq("type", type))
						.list(); 
		if(warnings.size()==0){
			return new WarnKitsNumb(type);
		}
		else{
			return warnings.get(0);
		}
	}

	@Override
	public void save(WarnKitsNumb wkn) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.saveOrUpdate(wkn);
		session.close();
	}

	@Override
	public void delete(WarnKitsNumb wkn) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.delete(wkn);
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WarnKitsNumb> getAllActived() throws HibernateException {
		return HibernateUtil.getSessionFactory().openSession().createCriteria(WarnKitsNumb.class)
				.add(Restrictions.eqOrIsNull("is_Deactived", false)).list();
	}
	
} 
