package reagents.data.warnings.kits;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

@Entity
@DynamicUpdate
@Table(name="warn_kits_sett")
@Proxy(lazy=false)
public class WarnKitsSett implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="type", unique=true, nullable=false)
	private String type;
	
	@Column(name="expiration")
	private int expiration;  
	
	@Column(name="count")
	private int minimalCnt;
	
	public WarnKitsSett(){		
	}

	public String getType() {
		return type;
	}

	public int getExpiration() {
		return expiration;
	}

	public int getMinimalCnt() {
		return minimalCnt;
	}

}
