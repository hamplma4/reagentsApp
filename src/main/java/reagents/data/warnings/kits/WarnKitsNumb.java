package reagents.data.warnings.kits;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

@Entity
@DynamicUpdate
@Table(name="warn_kits_numb")
@Proxy(lazy=false)
public class WarnKitsNumb implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="type", unique=true, nullable=false)
	private String type;
	
	@Column(name="is_deactivated")
	private boolean isDeactivated;
	
	public WarnKitsNumb(){		
	}

	public WarnKitsNumb(String type) {
		this.type = type;
		this.isDeactivated = false;
	}

	public String getType() {
		return type;
	}

	public boolean isDeactivated() {
		return isDeactivated;
	}
	
}
