package reagents.data.warnings.kits;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

@Entity
@DynamicUpdate
@Table(name="warn_kits_expi")
@Proxy(lazy=false)
public class WarnKitsExpi implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="kit_id", unique=true, nullable=false)
	private int kitId;
	
	@Column(name="is_deactivated")
	private boolean isDeactivated;  
	
	public WarnKitsExpi(){		
	}
	
	public WarnKitsExpi(int kitId){
		this.kitId = kitId;
		this.isDeactivated = false;
	}

	public int getKitId() {
		return kitId;
	}

	public void setKitId(int kitId) {
		this.kitId = kitId;
	}

	public boolean isDeactivated() {
		return isDeactivated;
	}

	public void setDeactivated(boolean isDeactivated) {
		this.isDeactivated = isDeactivated;
	}
	
}
