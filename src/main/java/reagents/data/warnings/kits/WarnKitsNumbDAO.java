package reagents.data.warnings.kits;

import java.util.List;

import org.hibernate.HibernateException;

public interface WarnKitsNumbDAO {
	
	public WarnKitsNumb getByType(String type) throws HibernateException;

	public void save(WarnKitsNumb wkn) throws HibernateException;

	public void delete(WarnKitsNumb wkn) throws HibernateException;

	public List<WarnKitsNumb> getAllActived() throws HibernateException;
	
}
