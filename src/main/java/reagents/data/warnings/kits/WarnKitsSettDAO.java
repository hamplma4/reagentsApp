package reagents.data.warnings.kits;

public interface WarnKitsSettDAO {
		
	WarnKitsSett getByType(String type);
	
}
