package reagents.data;

import java.util.List;

import org.hibernate.HibernateException;

@SuppressWarnings("rawtypes")
public interface DAOTemplate{
	public List getAll() throws HibernateException;
	public List reload(List selected) throws HibernateException;
	public void lock(List forLock, String user) throws HibernateException;
	public void update(List forUpdate) throws HibernateException;
	public void save(List forSave) throws HibernateException;
	public List getNonConsumed() throws HibernateException;
	public void unlock(List forUnlock) throws HibernateException;
}
