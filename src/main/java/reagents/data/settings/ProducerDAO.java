package reagents.data.settings;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import reagents.app.exception.DbIOException;
import reagents.app.hibernate.HibernateUtil;
import reagents.data.DAO;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ProducerDAO extends DAO{

	/* --------------------------------------------------------------- */
	public Producer getByName(String name) throws DbIOException{
		List<Producer> list;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Criteria criteria = session
					.createCriteria(Producer.class)
					.add(Restrictions.eq("name",name));
			list = criteria.list();
		}
		catch (HibernateException ex){
			throw new DbIOException(ex);
		}
		if(list.isEmpty()) return null;
		else return list.get(0);
	}
}
