package reagents.data.settings;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate; 
import org.hibernate.annotations.Proxy;
import reagents.app.exception.DbIOException;

@Entity
@DynamicUpdate
@Table(name="discard_options")
@Proxy(lazy=false)
public class DiscardOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	private int id;

	@Column(name="text" , unique=true, nullable=false ,length = 64)
	private String text;

	public DiscardOption(){}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

	public static DiscardOption create(String discardOption) throws DbIOException {
		DiscardOption d = new DiscardOption();
		d.text = discardOption;
		d.id = (new DiscardOptionsDAO()).getNextId(DiscardOption.class);
		return d;
	}
}
