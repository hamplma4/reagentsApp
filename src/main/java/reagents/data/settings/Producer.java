package reagents.data.settings;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;
import reagents.app.exception.DbIOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@DynamicUpdate
@Table(name="producers")
@Proxy(lazy=false)
public class Producer {

    @Id
    @Column(name="id", unique=true, nullable=false)
    public int id;

    @Column(name="name" , length=63)
    private String name;

    public Producer(){
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Producer create(String name) throws DbIOException{
        Producer p = new Producer();
        p.name = name;
        p.id = (new ProducerDAO()).getNextId(Producer.class);
        return p;
    }
}
