package reagents.data.settings;


import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import reagents.app.exception.DbIOException;
import reagents.app.hibernate.HibernateUtil;
import reagents.data.DAO;

import javax.inject.Singleton;

@Singleton
public class DiscardOptionsDAO extends DAO implements Serializable{

	/* ---------------------------------------------------------------- */
	public DiscardOption getByName(String name) throws DbIOException{
		List<DiscardOption> list;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Criteria criteria = session
					.createCriteria(DiscardOption.class)
					.add(Restrictions.eq("text",name));
			list = criteria.list();
		}
		catch (HibernateException ex){
			throw new DbIOException(ex);
		}
		if(list.isEmpty()) return null;
		else return list.get(0);
	}
}
