package reagents.data;

import org.apache.poi.ss.formula.functions.T;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import reagents.app.exception.DbIOException;
import reagents.app.hibernate.HibernateUtil;

import java.io.Serializable;
import java.util.List;

public class DAO implements Serializable{

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T> List<T> getAll(final Class<T> type) throws DbIOException{
        final Session session = HibernateUtil.getSessionFactory().openSession();
        final Criteria crit = session.createCriteria(type);
        List<T> list;
        try {
            list = crit.list();
        }
        catch (HibernateException ex){
            throw new DbIOException(ex);
        }
        return list;
    }

    public <T> void save(final T o) throws DbIOException{
        try {
            Session s = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.getTransaction();
            tx.begin();
            s.save(o);
            tx.commit();
        }
        catch (HibernateException ex){
            throw new DbIOException(ex);
        }
    }

    public void delete(final Object object) throws DbIOException{
        try {
            Session s = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = s.getTransaction();
            tx.begin();
            s.delete(object);
            tx.commit();
        }
        catch (HibernateException ex){
            throw new DbIOException(ex);
        }
    }

    public <T> int getNextId(final Class<T> type) throws DbIOException {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session
                    .createCriteria(type)
                    .setProjection(Projections.max("id"));
            Integer maxId = (Integer)criteria.uniqueResult();
            if(maxId==null) return 0;
            return maxId+1;
        } catch (HibernateException ex){
            throw  new DbIOException(ex);
        }
    }
}