package reagents.data.reagent;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import reagents.app.hibernate.HibernateUtil;

public class ReagentDAO{

	/* ---------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public List<Reagent> getAll() throws HibernateException {
		return (List<Reagent>)HibernateUtil
					.getSessionFactory()
					.openSession()
					.createCriteria(Reagent.class).list();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List reload(List selected) throws HibernateException {
		Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Reagent.class);
		Disjunction disjunction = Restrictions.disjunction();
		for(Reagent r: (List<Reagent>)selected){
				disjunction.add(Restrictions.eq("id", r.getId()));
		}
		criteria.add(disjunction);
		return criteria.list();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void lock(List forLock, String userName) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Reagent r: (List<Reagent>)forLock){
			r.lock(userName);
			session.update(r);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void update(List forUpdate) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Reagent r: (List<Reagent>)forUpdate){
			r.unLock();
			session.update(r);
		}
		session.getTransaction().commit();
		session.close();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void save(List forSave) throws HibernateException {
		int id = getNextId();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for (Reagent r: (List<Reagent>)forSave){
			r.setId(id);
			session.save(r);
			id++;
		}
		tx.commit();	
	}
	/* ---------------------------------------------------------------- */
	private int getNextId() throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session
			    .createCriteria(Reagent.class)
			    .setProjection(Projections.max("id"));
		Integer maxId = (Integer)criteria.uniqueResult();
		if(maxId==null) return 0;
		return maxId+1;
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings("rawtypes")
	public List getNonConsumed() throws HibernateException{
		return HibernateUtil.getSessionFactory().openSession()
							.createCriteria(Reagent.class)
							.add(Restrictions.isNull("consumed"))
							.list();
	}
	/* ---------------------------------------------------------------- */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void unlock(String userName) throws HibernateException {
		List<Reagent> list = HibernateUtil.getSessionFactory().openSession()
								.createCriteria(Reagent.class)
								.add(Restrictions.eq("editedBy",userName))
								.list();
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Reagent r: list){
			r.unLock();
			session.update(r);
		}
		session.getTransaction().commit();
		session.close();
	}
}
