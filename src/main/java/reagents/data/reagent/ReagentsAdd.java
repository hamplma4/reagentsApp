package reagents.data.reagent;

import java.util.Date;

public class ReagentsAdd {
	
	private int 	count;
	private String 	type;
	private String 	batch;
	private Date 	expiration;
	private Date 	arriveToLab;
	private Date 	validation;
	
	public ReagentsAdd(String type) {
		this.count = 1;
		this.type = type;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Date getArriveToLab() {
		return arriveToLab;
	}

	public void setArriveToLab(Date arriveToLab) {
		this.arriveToLab = arriveToLab;
	}

	public Date getValidation() {
		return validation;
	}

	public void setValidation(Date validation) {
		this.validation = validation;
	}
	
	
}
