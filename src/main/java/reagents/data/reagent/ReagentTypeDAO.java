package reagents.data.reagent;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.boot.model.relational.Database;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import reagents.app.exception.DbIOException;
import reagents.app.hibernate.HibernateUtil;
import reagents.data.DAO;

import java.util.List;

public class ReagentTypeDAO extends DAO {

    /* --------------------------------------------------------------- */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public ReagentType getByName(String name) throws DbIOException {
        List<ReagentType> list;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session
                    .createCriteria(ReagentType.class)
                    .add(Restrictions.eq("name",name));
            list = criteria.list();
        }
        catch (HibernateException ex){
            throw new DbIOException(ex);
        }
        if(list.isEmpty()) return null;
        else return list.get(0);
    }
    /* --------------------------------------------------------------- */

}
