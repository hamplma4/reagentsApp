package reagents.data.reagent;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

import reagents.app.ItemTemplate;
import reagents.app.ServiceTemplate;
import reagents.app.reagent.ReagentService;

@Entity
@DynamicUpdate
@Table(name="reagents")
@Proxy(lazy=false)
public class Reagent extends ItemTemplate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	private int id;

	@Column(name="internalId", length=64)
	private String internalId;

	@Column(name="batch", length=64)
	private String batch;

	@Column(name="type", length=64)
	private String type;

	@Column(name="expiration")
	private Date expiration;
	
	@Column(name="newExpiration")
	private Date newExpiration;
	
	@Column(name="consumed")
	private Date consumed;
	
	@Column(name="editedDate")
	private Date editedDate;
	
	@Column(name="editedBy", length=64)
	private String editedBy;
	
	@Column(name="discardReason", length=64)
	private String discardReason;
	
	@Column(name="arriveToLab")
	private Date arriveToLab;
	
	@Column(name="opened")
	private Date opened;
	
	@Column(name="validation")
	private Date validation;
	
	@Column(name="revalidation")
	private Date revalidation;

	@Column(name="manual", length=64)
	private String manual;

	@Column(name="note", length=128)
	private String note;

	@Transient
	private Reagent backUp;

	public Reagent(){}
	
	public Reagent(ReagentsAdd item) {
		super();
		this.type = item.getType();
		this.batch = item.getBatch();
		this.expiration = item.getExpiration();
		this.arriveToLab = item.getArriveToLab();
		this.validation = item.getValidation();
	}

	public Reagent(Reagent r) {
		this.id = r.id;
		this.internalId = r.internalId;
		this.batch = r.batch;
		this.type = r.type;
		this.expiration = r.expiration;
		this.newExpiration = r.newExpiration;
		this.consumed = r.consumed;
		this.editedDate = r.editedDate;
		this.editedBy = r.editedBy;
		this.discardReason = r.discardReason;
		this.arriveToLab = r.arriveToLab;
		this.opened = r.opened;
		this.validation = r.validation;
		this.revalidation = r.revalidation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Date getConsumed() {
		return consumed;
	}

	public void setConsumed(Date consumed) {
		this.consumed = consumed;
	}
	
	public String getDiscardReason() {
		return discardReason;
	}

	public void setDiscardReason(String discardReason) {
		this.discardReason = discardReason;
	}

	public Date getArriveToLab() {
		return arriveToLab;
	}

	public void setArriveToLab(Date arriveToLab) {
		this.arriveToLab = arriveToLab;
	}

	public Date getOpened() {
		return opened;
	}

	public void setOpened(Date opened) {
		this.opened = opened;
	}

	public Date getValidation() {
		return validation;
	}

	public void setValidation(Date validation) {
		this.validation = validation;
	}

	public Date getNewExpiration() {
		return newExpiration;
	}

	public void setNewExpiration(Date newExpiration) {
		this.newExpiration = newExpiration;
	}

	public Date getRevalidation() {
		return revalidation;
	}

	public void setRevalidation(Date revalidation) {
		this.revalidation = revalidation;
	}

	public Reagent getBackUp() {
		return backUp;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}

	public boolean isLocked(String userName){
		
		if (editedBy == null || editedBy.equals("") || editedBy.equals(userName) ) return false;
		if (editedDate == null) return false;
		if (editedDate.getTime() + 900000 < System.currentTimeMillis()) return false;
		return true;
	}

	public ServiceTemplate getService(){
		return new ReagentService();
	}

	public void createBackUp(){
		backUp = new Reagent(this);
	}

	public void lock(String userName){
		editedBy = userName;
		editedDate = new Date();
	}

	public void unLock(){
		editedBy = "";
		editedDate = null;
	}
	
}
