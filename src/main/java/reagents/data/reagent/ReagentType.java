package reagents.data.reagent;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;
import reagents.app.exception.DbIOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@DynamicUpdate
@Table(name="reagentTypes")
@Proxy(lazy=false)
public class ReagentType implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id", unique=true, nullable=false)
    private int id;

    @Column(name="name", unique=true, nullable=false, length = 64)
    private String name;

    public ReagentType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ReagentType create(String reagentType) throws DbIOException {
        ReagentType type = new ReagentType();
        type.id = (new ReagentTypeDAO()).getNextId(ReagentType.class);
        type.name = reagentType;
        return type;
    }
}
