package reagents.data.kit;

import java.io.Serializable;
import java.util.Date;
import java.util.ResourceBundle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

import reagents.app.ItemTemplate;
import reagents.app.ServiceTemplate;
import reagents.app.kit.KitService;
import reagents.data.order.OrderItem;

@Entity
@DynamicUpdate
@Table(name="kits")
@Proxy(lazy=false)
public class Kit extends ItemTemplate implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	public int id;
	
	@Column(name="type" , length=63)
	private String type;
	
	@Column(name="internalId", length=63)
	private String internalId;
	
	@Column(name="batch", length=63, nullable=false)
	private String batch;
	
	@Column(name="expiration")
	private Date expiration;
	
	@Column(name="arriveToLab")
	private Date arriveToLab;

	@Column(name="released")
	private Date validation;
	
	@Column(name="opened")
	private Date opened;

	@Column(name="consumed")
	private Date consumed;
	
	@Column(name="revalidation")
	private Date revalidation;
	
	@Column(name="newExpiration")
	private Date newExpiration;
	
	@Column(name="manual", length=63)
	private String manual;
	
	@Column(name="note", length=63)
	private String note;
	
	@Column(name="discardReason", length=63)
	private String discardReason;
	
	@Column(name="editedDate")
	private Date editedDate;
	
	@Column(name="editedBy", length=63)
	private String editedBy;
	
	@Transient
	private Kit backUp;
		
	public Kit getBackUp() {
		return backUp;
	}
	
	public Kit(){
		
	}

	public Kit(Kit kit) {
		super();
		this.id = kit.id;
		this.type = kit.type;
		this.internalId = kit.internalId;
		this.batch = kit.batch;
		this.expiration = kit.expiration;
		this.arriveToLab = kit.arriveToLab;
		this.validation = kit.validation;
		this.opened = kit.opened;
		this.consumed = kit.consumed;
		this.revalidation = kit.revalidation;
		this.newExpiration = kit.newExpiration;
		this.manual = kit.manual;
		this.note = kit.note;
		this.discardReason = kit.discardReason;
		this.editedDate = kit.editedDate;
		this.editedBy = kit.editedBy;
		this.backUp = kit.backUp;
	}
	
	public Kit(KitsAdd item) {
		super();
		this.type = item.getType();
		this.batch = item.getBatch();
		this.expiration = item.getExpiration();
		this.arriveToLab = item.getArriveToLab();
		this.validation = item.getValidation();
		this.manual = item.getManual();
	}
	
	public Kit(int kitId) {
		this.id = kitId;
	}

	public Kit(OrderItem item) {
		String type = item.getType();
		// delete excl.
		int i = type.indexOf("excl.");
		if(i!=-1){
			this.type = type.substring(0,i-1);
		}
		else {
			this.type = type;
		}
		this.type = this.type.trim();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public java.util.Date getExpiration() {
		return expiration;
	}

	public void setExpiration(java.util.Date expiration) {
		this.expiration = expiration;
	}
  
	public Date getArriveToLab() {
		return arriveToLab;
	}

	public void setArriveToLab(Date arriveToLab) {
		this.arriveToLab = arriveToLab;
	}

	public Date getValidation() {
		return validation;
	} 

	public void setValidation(Date released) {
		this.validation = released;
	}

	public Date getOpened() {
		return opened;
	}

	public void setOpened(Date opened) {
		this.opened = opened;
	}

	public Date getConsumed() {
		return consumed;
	}

	public void setConsumed(Date consumed) {
		this.consumed = consumed;
	}

	public Date getRevalidation() {
		return revalidation;
	}

	public void setRevalidation(Date revalidation) {
		this.revalidation = revalidation;
	}

	public Date getNewExpiration() {
		return newExpiration;
	}

	public void setNewExpiration(Date newExpiration) {
		this.newExpiration = newExpiration;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDiscardReason() {
		return discardReason;
	}

	public void setDiscardReason(String discardReason) {
		this.discardReason = discardReason;
	}

	@Override
	public String getEditedBy() {
		return editedBy;
	}

	/*
	public boolean isExpired(WarnKitsSett setting) {
		final int dayInMillis = 86400000;
		final int minimumLife = setting.getExpiration();
		final Date expDate = this.getYourExpiration();
		if(expDate!=null && expDate.getTime()-minimumLife*dayInMillis<System.currentTimeMillis()){
			return true;
		}
		return false;
	}*/

	/*
	private Date getYourExpiration() {
		if(newExpiration!=null) return newExpiration;
		if(expiration!=null) return expiration;
		return null;
	}*/

	@Override
	public boolean isLocked(String userName){
		if (editedBy == null || editedBy.equals(userName) || editedBy.equals(userName) ) return false;
		if (editedDate == null) return false;
		if (editedDate.getTime() + 900000 > System.currentTimeMillis()) return false;
		return true;
	}

	@Override
	public void createBackUp(){
		backUp = new Kit(this);
	}

	@Override
	public ServiceTemplate getService(){
		return new KitService();
	}

	public void lock(String userName){
		editedBy = userName;
		editedDate = new Date();
	}

	public void unlock(){
		editedBy = "";
		editedDate = null;
	}

	public void exportSelf(XSSFRow row, CellStyle cellDateStyle) {

		XSSFCell cell;
		cell = row.createCell(0);
		cell.setCellValue(id);
		cell = row.createCell(1);
		cell.setCellValue(type);
		cell = row.createCell(2);
		cell.setCellValue(internalId);
		cell = row.createCell(3);
		cell.setCellValue(batch);
		cell = row.createCell(4);
		cell.setCellStyle(cellDateStyle);
		if(expiration!=null) cell.setCellValue(expiration);
		else cell.setCellValue("");
		cell = row.createCell(5);
		cell.setCellStyle(cellDateStyle);
		if(arriveToLab!=null) cell.setCellValue(arriveToLab);
		else cell.setCellValue("");
		cell = row.createCell(6);
		cell.setCellStyle(cellDateStyle);
		if(validation!=null) cell.setCellValue(validation);
		else cell.setCellValue("");
		cell = row.createCell(7);
		cell.setCellStyle(cellDateStyle);
		if(opened!=null) cell.setCellValue(opened);
		else cell.setCellValue("");
		cell = row.createCell(8);
		cell.setCellStyle(cellDateStyle);
		if(consumed!=null) cell.setCellValue(consumed);
		else cell.setCellValue("");
		cell = row.createCell(9);
		cell.setCellStyle(cellDateStyle);
		if(revalidation!=null) cell.setCellValue(revalidation);
		else cell.setCellValue("");
		cell = row.createCell(10);
		cell.setCellStyle(cellDateStyle);
		if(newExpiration!=null) cell.setCellValue(newExpiration);
		else cell.setCellValue("");
		cell = row.createCell(11);
		cell.setCellValue(manual);
		cell = row.createCell(12);
		cell.setCellValue(note);
		cell = row.createCell(13);
		cell.setCellValue(discardReason);
	}

	public static void exportHeader(XSSFRow row){
		ResourceBundle cz = ResourceBundle.getBundle("text_cz");

		XSSFCell cell;
		cell = row.createCell(0);
		cell.setCellValue("id");
		cell = row.createCell(1);
		cell.setCellValue(cz.getString("type"));
		cell = row.createCell(2);
		cell.setCellValue(cz.getString("intId"));
		cell = row.createCell(3);
		cell.setCellValue(cz.getString("batch"));
		cell = row.createCell(4);
		cell.setCellValue(cz.getString("expiration"));
		cell = row.createCell(5);
		cell.setCellValue(cz.getString("arriveToLab"));
		cell = row.createCell(6);
		cell.setCellValue(cz.getString("validation"));
		cell = row.createCell(7);
		cell.setCellValue(cz.getString("opened"));
		cell = row.createCell(8);
		cell.setCellValue(cz.getString("consumed"));
		cell = row.createCell(9);
		cell.setCellValue(cz.getString("revalidation"));
		cell = row.createCell(10);
		cell.setCellValue(cz.getString("newExpiration"));
		cell = row.createCell(11);
		cell.setCellValue(cz.getString("manual"));
		cell = row.createCell(12);
		cell.setCellValue(cz.getString("note"));
		cell = row.createCell(13);
		cell.setCellValue(cz.getString("discardReason"));

	}
}