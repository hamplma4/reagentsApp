package reagents.data.kit;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import reagents.app.hibernate.HibernateUtil;
import reagents.data.DAO;

import javax.inject.Singleton;

@Singleton
public class KitDAO extends DAO{

	@SuppressWarnings("rawtypes")
	public List getNonConsumed() throws HibernateException{
		return HibernateUtil.getSessionFactory().openSession()
							.createCriteria(Kit.class)
							.add(Restrictions.isNull("consumed"))
							.list();
	}
	/* --------------------------------------------------------------- */
	private int getNextId() throws HibernateException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session
			    .createCriteria(Kit.class)
			    .setProjection(Projections.max("id"));
		Integer maxId = (Integer)criteria.uniqueResult();
		return maxId+1;
	}
	/* ------------------------------------------------------------ */

	/* ------------------------------------------------------------ */
	/*public List<Kit> getKitsByType(String type, boolean onlyUnArchived) throws HibernateException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Kit.class);
		criteria.add(Restrictions.eq("kit_type", type));
		if(onlyUnArchived){
			criteria.add(Restrictions.isNotNull("kit_consumed"));
		}
		@SuppressWarnings("unchecked")
		List<Kit> kits = (List<Kit>)criteria.list();
		return kits;
	}*/
	/* ------------------------------------------------------------ */
	/*public List<String> getKitTypes(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Kit.class);
		criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("kit_type")));
        @SuppressWarnings("unchecked")
		List<String> res = criteria.list();        
        for (String item : res) {
        	System.out.println(item);
        }		  
        return res;		   
        
	}*/
	/* ------------------------------------------------------------ */
	/*
	public List<Kit> getByIds(List<WarnKitsExpi> allActived) throws HibernateException{
		List<Kit> kits = new ArrayList<Kit>();
		for(WarnKitsExpi warn: allActived){
			kits.add(new Kit(warn.getKitId()));
		}
		return reload(kits);
	}*/
	/* ------------------------------------------------------------ */
	@SuppressWarnings("rawtypes")
	public List getAll() throws HibernateException {
		return HibernateUtil.getSessionFactory().openSession()
				.createCriteria(Kit.class)
				.list();
	}
	/* ------------------------------------------------------------ */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List reload(List selected) throws HibernateException {
		Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Kit.class);
		Disjunction disjunction = Restrictions.disjunction();
		if(selected != null){
			for(Kit k: (List<Kit>)selected){
					disjunction.add(Restrictions.eq("id", k.getId()));
			}
		}	
		criteria.add(disjunction);
		return criteria.list();
	}
	/* ------------------------------------------------------------ */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void lock(List forLock, String user) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Kit k: (List<Kit>)forLock){
			k.lock(user);
			session.update(k);
		}
		session.getTransaction().commit();
		session.close();
		
	}
	/* ------------------------------------------------------------ */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void update(List forUpdate) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Kit k: (List<Kit>)forUpdate){
			k.unlock();
			session.update(k);
		}
		session.getTransaction().commit();
		session.close();
		
	}
	/* ------------------------------------------------------------ */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void save(List forSave) throws HibernateException {
		int id = getNextId();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		for (Kit k: (List<Kit>)forSave){
			k.setId(id);
			session.save(k);
			id++;
		}
		tx.commit();	
	}
	/* ------------------------------------------------------------ */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void unlock(String userName) {
		List<Kit> list = HibernateUtil.getSessionFactory().openSession()
				.createCriteria(Kit.class)
				.add(Restrictions.eq("editedBy",userName))
				.list();
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for(Kit k: list){
			k.unlock();
			session.update(k);
		}
		session.getTransaction().commit();
		session.close();
	}


	/* ------------------------------------------------------------ */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getByTypeStart(String type) {
		return HibernateUtil.getSessionFactory().openSession()
				.createCriteria(Kit.class)
				.add(Restrictions.like("type",type))
				.list();
	}
}
