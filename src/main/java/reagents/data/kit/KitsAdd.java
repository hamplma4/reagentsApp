package reagents.data.kit;

import java.util.Date;

public class KitsAdd {
	
	private int 	countOfKits;
	private String 	type;
	private String 	batch;
	private Date 	expiration;
	private Date 	arriveToLab;
	private Date 	validation;
	private String 	manual;
	
	public KitsAdd() {
		this.countOfKits = 1;
	}

	public int getCountOfKits() {
		return countOfKits;
	}

	public void setCountOfKits(int countOfKits) {
		this.countOfKits = countOfKits;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Date getArriveToLab() {
		return arriveToLab;
	}

	public void setArriveToLab(Date arriveToLab) {
		this.arriveToLab = arriveToLab;
	}

	public Date getValidation() {
		return validation;
	}

	public void setValidation(Date validation) {
		this.validation = validation;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}	
	
}
