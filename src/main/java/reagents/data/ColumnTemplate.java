package reagents.data;

import java.io.Serializable;

public class ColumnTemplate implements Serializable {
	 
	private static final long serialVersionUID = 1L;
	private String header;
    private String property;
    private boolean toggleable;
    private boolean visible;

    public ColumnTemplate(String header, String property, boolean toggleable, boolean visible) {
        this.header = header;
        this.property = property;
    }

    public String getHeader() {
        return header;
    }

    public String getProperty() {
        return property;
    }
    
    public String getFilterMatchMode(){
    	return "contains";
    }

	public boolean isToggleable() {
		return toggleable;
	}

	public void setToggleable(boolean toggleable) {
		this.toggleable = toggleable;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
}
