package reagents.app;

import java.util.List;

import org.hibernate.HibernateException;
import reagents.app.exception.DbIOException;

@SuppressWarnings("rawtypes")
public abstract class ServiceTemplate {
	
	public abstract List getAll() throws HibernateException, DbIOException;
	
	public abstract List reload(List selected) throws HibernateException;

	public abstract void lock(List forLock,String user) throws HibernateException;

	public abstract void unlock(String userName) throws HibernateException;
	
	public abstract void update(List forUpdate) throws HibernateException;
	
	public abstract void save(List forSave) throws HibernateException;

	public abstract List initCurrent() throws HibernateException, DbIOException;

}
