package reagents.app.kit;

import java.util.List;

import reagents.data.kit.Kit;

import javax.inject.Singleton;

public class KitFunctions {
	
	private String userName;
	
	public KitFunctions(String userName) {
		this.userName = userName;
	}

	public String checkIfLocked(List<Kit> kits) {
		for (Kit k : kits) {
			if(k.isLocked(userName)){
				return k.getEditedBy();
			}
		}
		return "No";
	}

	public boolean checkIfConsumed(List<Kit> edited) {
		for(Kit k: edited){
			if(k.getConsumed() != null){
				return true;
			}
		}
		return false;
	}

	public boolean checkIfOpened(List<Kit> edited) {
		for (Kit k : edited) {
			if (k.getOpened() != null) {
				return true;
			}
		}
		return false;
	}
	
}
