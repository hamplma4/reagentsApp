package reagents.app.kit.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.kit.KitFunctions;
import reagents.app.login.bean.LoginBean;
import reagents.app.view.DataView;
import reagents.app.Msg;
import reagents.data.kit.*;
import reagents.data.settings.DiscardOptionsDAO;

@SessionScoped
@Named
public class KitBean implements Serializable {

	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	private DataView<Kit> view;
	public DataView<Kit> getView() {
		return view;
	}
	/* ------------------------------------------------------------------------- */
	/* Add Kits Variables */
	private List<KitsAdd> add;
	public List<KitsAdd> getKitAdd() {
		return add;
	}
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	private LoginBean loginBean;
	/* ------------------------------------------------------------------------- */
	private ResourceBundle cz;
	private String userName;
	private KitFunctions func;
	DiscardOptionsDAO discardOptionsDAO;
	/* ------------------------------------------------------------------------- */
	public void init() throws LoginExpiredExeption, DbIOException {
		if(userName==null) {
			userName = loginBean.getLoggedUserName();
		}
		if(view == null){
			view = new DataView<>(new Kit(),userName);
		}
		if(func==null){
			func = new KitFunctions(userName);
		}
		view.initCurrent();
		if(cz == null){
			cz = loginBean.getCz();
		}
	}
	/* ------------------------------------------------------------------------- */
	@Inject
	public KitBean (LoginBean loginBean) throws LoginExpiredExeption, DbIOException {
		this.loginBean = loginBean;
		discardOptionsDAO = new DiscardOptionsDAO();
		init();
	}
	/* ------------------------------------------------------------------------- */
	private boolean showConsumed=false;
	public boolean isShowConsumed() {
		return showConsumed;
	}
	/* ------------------------------------------------------------------------- */
	private String editable;
	public String getEditable() {
		return editable;
	}
	/* ------------------------------------------------------------------------- */
	public String editNext() {
		editable = "false";
		Msg.create(cz.getString("info"),cz.getString("checkIt"),FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("kitEdit:table");
		return "";
	}
	/* ------------------------------------------------------------------------- */
	public String editPrev() {
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("kitEdit:table");
		return "edit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String edit() {
		if(view.getSelected().size()==0) {
			Msg.create(cz.getString("warning"), cz.getString("youMustSelectSomething"), FacesMessage.SEVERITY_WARN, false);
			return "";
		}
		view.reload();
		String result = func.checkIfLocked(view.getEdited());
		if (!result.equals("No")) {
			Msg.create(cz.getString("error"),cz.getString("isLockedBy")+" "+result+".",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.lock();
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "edit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String openNext() {
		editable = "false";
		Msg.create(cz.getString("info"),cz.getString("checkIt"),FacesMessage.SEVERITY_INFO,true);
		return "open?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String openPrev() {
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "open?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String open() {
		if(view.getSelected().size()==0) {
			Msg.create(cz.getString("warning"),cz.getString("youMustSelectSomething"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.reload();
		String result = func.checkIfLocked(view.getEdited());
		if (!result.equals("No")) {
			Msg.create(cz.getString("error"),cz.getString("isLockedBy")+" "+result+".",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		if (func.checkIfOpened(view.getEdited())) {
			Msg.create(cz.getString("warning"),cz.getString("alreadyOpenedKit"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.lock();
		for(Kit k: view.getEdited()){
			k.setOpened(new Date());
		}
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "open?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String consumeNext() {
		editable = "false";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,false);
		return "consume?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String consumePrev() {
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("checkIt"),FacesMessage.SEVERITY_INFO,false);
		return "consume?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String consume() {
		if(view.getSelected().size()==0) {
			Msg.create(cz.getString("warning"),cz.getString("youMustSelectSomething"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.reload();
		String result = func.checkIfLocked(view.getEdited());
		if (!result.equals("No")) {
			Msg.create(cz.getString("error"),cz.getString("isLockedBy")+" "+result+".",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		if (!func.checkIfConsumed(view.getEdited())) {
			Msg.create(cz.getString("warning"),cz.getString("alreadyConsumedKit"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.lock();
		for(Kit k: view.getEdited()){
			k.setConsumed(new Date());
			k.setDiscardReason("Spotřebováno");
		}
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "consume?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String update() {
		view.update();
		Msg.create(cz.getString("saved"),"",FacesMessage.SEVERITY_INFO,true);
		return "kit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String add() {
		add = new ArrayList<>();
		add.add(new KitsAdd());
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "add?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String addRow() {
		add.add(new KitsAdd());
		Msg.create(cz.getString("rowAdded"),"",FacesMessage.SEVERITY_INFO,true);
		return "add?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String addSpecific() {
		if (add == null) {
			return "kit?faces-redirect=true";
		}
		view.setEdited(new ArrayList<>());
		for (KitsAdd item : add) {
			for (int i = 0; i < item.getCountOfKits(); i++) {
				view.getEdited().add(new Kit(item));
			}
		}
		editable = "true";
		Msg.create(cz.getString("fillIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "addSpecific?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String addNext() {
		editable = "false";
		Msg.create(cz.getString("checkIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "addSpecific?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String addPrev() {
		if(editable.equals("true")){
			Msg.create(cz.getString("fillIt"),"",FacesMessage.SEVERITY_INFO,true);
			return "add?faces-redirect=true";
		}
		else{
			editable = "true";
			Msg.create(cz.getString("fillSpecific"),"",FacesMessage.SEVERITY_INFO,true);
			RequestContext.getCurrentInstance().update("kitEdit:table");
			return "";
		}
	}
	/* ------------------------------------------------------------------------- */
	public String save() {
		add.clear();
		view.save();
		Msg.create(cz.getString("saved"),"",FacesMessage.SEVERITY_INFO,true);
		return "kit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String cancel() {
		view.unlock();
		Msg.create(cz.getString("unlocked"),"",FacesMessage.SEVERITY_INFO,true);
		return "kit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public void showAll() throws DbIOException{
		if(!showConsumed){
			showConsumed = true;
			view.initAll();
			Msg.create(cz.getString("info"),cz.getString("consumedIsShowed"),FacesMessage.SEVERITY_INFO,false);
		}
		else{
			showConsumed = false;
			view.initCurrent();
			Msg.create(cz.getString("info"),cz.getString("consumedIsHidden"),FacesMessage.SEVERITY_INFO,false);
		}
		RequestContext.getCurrentInstance().update("kit:table");
	}
	/* ------------------------------------------------------------------------- */
	private List discardOptions;
	public List getDiscardOptions() throws DbIOException{
		if(discardOptions==null) {
			discardOptions = discardOptionsDAO.getAll(DiscardOptionsDAO.class);
		}
		return discardOptions;
	}
}