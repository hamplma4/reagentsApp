package reagents.app.kit;

import java.util.List;

import org.hibernate.HibernateException;

import reagents.app.ServiceTemplate;
import reagents.data.kit.KitDAO;

public class KitService extends ServiceTemplate{
	
	@SuppressWarnings("rawtypes")
	@Override
	public synchronized List getAll() throws HibernateException {
		return (new KitDAO()).getAll();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized List reload(List selected) throws HibernateException {
		return (new KitDAO()).reload(selected);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized void lock(List forLock, String user) throws HibernateException {
		(new KitDAO()).lock(forLock, user);	
	}

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized void update(List forUpdate) throws HibernateException {
		(new KitDAO()).update(forUpdate);	
	} 

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized void save(List forSave) throws HibernateException {
		(new KitDAO()).save(forSave);	
	}

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized void unlock(String userName) throws HibernateException {
		(new KitDAO()).unlock(userName);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public synchronized List initCurrent() throws HibernateException {
		return (new KitDAO()).getNonConsumed();
	}

	public synchronized List getByTypeStart(String type) throws HibernateException {
		return (new KitDAO()).getByTypeStart(type);
	}
	
}
