package reagents.app.login.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import java.util.ResourceBundle;

import reagents.app.Msg;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.user.UserFunctions;
import reagents.data.user.User;
import reagents.data.user.UserDAO;

@SessionScoped
@Named
public class LoginBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ResourceBundle cz;
	private User user;
	public String getLoggedUserName() throws LoginExpiredExeption{
		if(user==null){
			throw new LoginExpiredExeption();
		}
		return user.getUserName();
	}
	public String getAllName(){
		return 	user.getDegreesBefore()+" "+
				user.getFirstName()+" "+
				user.getLastName()+" "+
				user.getDegreesAfter();
	}
	public ResourceBundle getCz (){
		return cz;
	}
	/* ------------------------------------------------------------------------- */
	/* Edit variables */
	private User 	editUser;
	private String 	editPasswd;
	private String  editDisabled;
	public  String 	getEditDisabled() {
		return editDisabled;
	}
	public  void 	setEditDisabled(String editDisabled) {
		this.editDisabled = editDisabled;
	}
	public  String 	getEditPasswd() {
		return editPasswd;
	}
	public  void 	setEditPasswd(String editPasswd) {
		this.editPasswd = editPasswd;
	}
	public 	User 	getEditUser() {
		return editUser;
	}
	public 	void 	setEditUser(User editUser) {
		this.editUser = editUser;
	}
	/* ------------------------------------------------------------- */
	/* Login variables */
	private String userName;
	private String password; 
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/* ------------------------------------------------------------- */
	public LoginBean (){
		cz = ResourceBundle.getBundle("text_cz");
	}
	/* ------------------------------------------------------------- */
	public String logout() {
		user = null;
		userName = null;
		password = null;
		return "/login?faces-redirect=true";
	}
	/* ------------------------------------------------------------- */
	public String userLogin() {
		user = (new UserDAO()).getByUserName(userName);
		if (user != null) {
			if(user.getPasswd().equals(UserFunctions.makeHash(password))){
				password = "";
				Msg.create(cz.getString("info"),cz.getString("loginSuccessful"),FacesMessage.SEVERITY_INFO , true);
				return "/pages/kits/kit?faces-redirect=true";
			}
		}
		password = "";
		Msg.create(cz.getString("error"),cz.getString("loginFailed"),
					FacesMessage.SEVERITY_ERROR , true);
		return "";
	}
	/* ------------------------------------------------------------- */
	public boolean securityCheck(String minimalRole) {
		int minRole = Integer.parseInt(minimalRole);
		return (user!=null && user.getRole() >= minRole);
	}
	/* ------------------------------------------------------------- */
	public String settings(){
		editUser = (new UserDAO()).getByUserName(user.getUserName());
		if(editUser.isLocked(user.getUserName())){
			Msg.create(cz.getString("userEditingByAdmin"),"",FacesMessage.SEVERITY_ERROR , false);
			return "";
		}
		editUser.lock(user.getUserName());
		editUser.createBackUp();
		editPasswd = "";
		editDisabled = "false";
		return "/pages/userSettings?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String editNext(){
		// Check if email is no already used
		if(!editUser.getEmail().equals(editUser.getBackUp().getEmail())
				&& !UserFunctions.checkUniqueMail(editUser)){
			Msg.create(cz.getString("mailIsUsed"),"", FacesMessage.SEVERITY_ERROR,false);
			return "";
		}
		// Password
		if(editPasswd.equals("")) editPasswd = cz.getString("passwdIsNotFilled");
		else{
			editUser.setPasswd(UserFunctions.makeHash(editPasswd));
			editPasswd = cz.getString("passwdIsFilled");
		}
		// Set and redirect
		editDisabled = "true";
		Msg.create(cz.getString("checkIt"),"", FacesMessage.SEVERITY_INFO,false);
		return "userSettings?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String editPrev(){
		// Set and redirect
		editDisabled = "false";
		editPasswd = "";
		Msg.create(cz.getString("fillIt"),"", FacesMessage.SEVERITY_INFO,false);
		return "userSettings?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String saveUser(){
		user.unlock();
		user.update();
		Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,true);
		return "kits/kit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String cancel(){
		user.unlock();
		Msg.create(cz.getString("unlocked"),"", FacesMessage.SEVERITY_INFO,false);
		return "kits/kit?faces-redirect=true";
	}
}