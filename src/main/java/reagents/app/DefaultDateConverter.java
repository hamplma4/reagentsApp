package reagents.app;

import java.util.TimeZone;

import javax.faces.convert.FacesConverter;

import org.primefaces.convert.DateTimeConverter;


@FacesConverter(forClass=java.util.Date.class)
public class DefaultDateConverter extends DateTimeConverter {

    public DefaultDateConverter() {
    	super();
    	setPattern("dd.MM.yyyy");  
    	setTimeZone(TimeZone.getDefault());
    }
    
    /*
    public Object getAsObject(javax.faces.context.FacesContext context, javax.faces.component.UIComponent component, String value){
    	System.out.println("getAsObject used");
    	return super.getAsObject(context, component, value);
    	
    }*/

}
