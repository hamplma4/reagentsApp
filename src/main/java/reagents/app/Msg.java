package reagents.app;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;

public class Msg implements Serializable{

    public static void create(String summary, String detail, FacesMessage.Severity severity, boolean nextPage){
        if(FacesContext.getCurrentInstance() == null) return; // Test condition
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, summary, detail));
        if(nextPage){
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
    }

}
