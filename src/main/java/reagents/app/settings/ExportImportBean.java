package reagents.app.settings;

import org.apache.poi.util.SystemOutLogger;
import org.primefaces.model.StreamedContent;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.data.DAO;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ResourceBundle;

@SessionScoped
@Named
public class ExportImportBean implements Serializable {

    private static final long serialVersionUID = 1L;
    /* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
    private LoginBean loginBean;
    /* ------------------------------------------------------------------------- */
    private ResourceBundle cz;
    private String userName;
    /* ------------------------------------------------------------------------- */
    public void init() throws LoginExpiredExeption {
        if(userName==null) {
            userName = loginBean.getLoggedUserName();
        }
        if(cz == null){
            cz = loginBean.getCz();
        }
    }
    /* ------------------------------------------------------------------------- */
    @Inject
    public ExportImportBean (LoginBean loginBean) throws LoginExpiredExeption {
        this.loginBean = loginBean;
        init();
    }
    /* ------------------------------------------------------------------------- */
    private StreamedContent file;

    public void download() throws IOException, DbIOException {
        System.out.println("method download called");
        file = Exporter.writeXLSXFile();
    }

    public StreamedContent getFile() throws IOException, DbIOException{
        return Exporter.writeXLSXFile();
    }
}