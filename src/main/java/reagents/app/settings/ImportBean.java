package reagents.app.settings;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.model.UploadedFile;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.app.view.DataView;
import reagents.data.kit.Kit;
import reagents.data.reagent.Reagent;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class ImportBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	private DataView<Kit> kits;
	private DataView<Reagent> reagents;
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	private LoginBean loginBean;
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	public LoginBean getLoginBean() {
		return loginBean;
	}
	/* ------------------------------------------------------------------------- */
	private String userName;
	@PostConstruct
	public void init() throws LoginExpiredExeption{
		if(userName==null){
			userName = loginBean.getLoggedUserName();
		}
	}
	/* ------------------------------------------------------------------------- */
	@Inject
	public ImportBean (LoginBean loginBean) throws LoginExpiredExeption{
		this.loginBean = loginBean;
		this.init();
	}
	/* ------------------------------------------------------------------------- */
    private UploadedFile file;
    public UploadedFile getFile() {
        return file;
    }
    public void setFile(UploadedFile file) {
        this.file = file;
    }
	/* ------------------------------------------------------------------------- */
	public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            try {
				kits.setEdited(this.importKits());
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
	/* ------------------------------------------------------------------------- */
	private List<Kit> importKits() throws IOException{
		InputStream f = file.getInputstream();
		HSSFWorkbook wb = new HSSFWorkbook(f);
		HSSFSheet ws = wb.getSheetAt(0);
		int rowNum = ws.getLastRowNum()+1;
		List<Kit> kits = new ArrayList<Kit>();
		for (int i=1; i<rowNum; i++){
			HSSFRow row = ws.getRow(i);
			kits.add(this.readKit(row));
		}
		return kits;
	}
	/* ------------------------------------------------------------------------- */
	private Kit readKit(HSSFRow row) {
		Kit k = new Kit();
		k.setType(row.getCell(1,HSSFRow.CREATE_NULL_AS_BLANK).getStringCellValue());
		if(!k.getType().equals("")){
			k.setBatch(row.getCell(2,HSSFRow.CREATE_NULL_AS_BLANK).getStringCellValue());
			k.setInternalId(row.getCell(3,HSSFRow.CREATE_NULL_AS_BLANK).getStringCellValue());
			k.setExpiration(row.getCell(3,HSSFRow.RETURN_BLANK_AS_NULL).getDateCellValue());
		}
		System.out.println(k.getType());
		System.out.println(k.getExpiration());
		return k;
	}
}