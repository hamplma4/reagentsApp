package reagents.app.settings;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import reagents.app.exception.DbIOException;
import reagents.data.kit.Kit;
import reagents.data.kit.KitDAO;

import java.io.*;
import java.util.List;


public class Exporter {

    public static StreamedContent writeXLSXFile() throws IOException, DbIOException {
        System.out.println("Exporter started");
        StreamedContent content = null;
        ByteArrayOutputStream outputStream = null;
        try {
            //now write the PDF content to the output stream
            byte[] bytes;
            outputStream = new ByteArrayOutputStream();

            XSSFWorkbook wb = new XSSFWorkbook();
            /* Exports kits */
            String sheetName = "Kity";//name of sheet
            XSSFSheet sheet = wb.createSheet(sheetName) ;
            CellStyle cellDateStyle = wb.createCellStyle();
            CreationHelper createHelper = wb.getCreationHelper();
            cellDateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.mm.yyyy"));
            // Load kits from DB
            List<Kit> kits = (new KitDAO()).getAll(Kit.class);
            System.out.println("Exporter started write");
            int rowIterator = 0;
            XSSFRow row = sheet.createRow(rowIterator);
            Kit.exportHeader(row);
            // TODO
            rowIterator++;
            for (Kit kit: kits)
            {
                row = sheet.createRow(rowIterator);
                kit.exportSelf(row,cellDateStyle);
                rowIterator++;
            }
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();

            bytes = outputStream.toByteArray();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            content = new DefaultStreamedContent(inputStream, "application/xlsx", "data.xlsx");
            System.out.println("pf_OrdersController.getContractPDFToDownload(): completed");
        } catch(Exception ex) {
            System.out.println("Error generating CONTRACT PDF file to download");
            ex.printStackTrace();
        } finally {
            //clean off
            if(null != outputStream) {
                try {
                    outputStream.close();
                }
                catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        System.out.println("Exporter end");
        return content;
    }

}
