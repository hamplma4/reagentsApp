package reagents.app.settings;

import org.primefaces.context.RequestContext;
import reagents.app.Msg;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.data.reagent.ReagentType;
import reagents.data.reagent.ReagentTypeDAO;
import reagents.data.settings.DiscardOption;
import reagents.data.settings.DiscardOptionsDAO;
import reagents.data.settings.Producer;
import reagents.data.settings.ProducerDAO;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

import java.util.List;
import java.util.ResourceBundle;

@SessionScoped
@Named
public class SettingsBean implements Serializable {

    private static final long serialVersionUID = 1L;
    /* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
    private LoginBean loginBean;
    /* ------------------------------------------------------------------------- */
    private ResourceBundle cz;
    private String userName;
    /* ------------------------------------------------------------------------- */
    public void init() throws LoginExpiredExeption {
        if(userName==null) {
            userName = loginBean.getLoggedUserName();
        }
        if(cz == null){
            cz = loginBean.getCz();
        }
    }
    /* ------------------------------------------------------------------------- */
    private String discardOption;
    private String producer;
    private String reagentType;
    public String getDiscardOption() {
        return discardOption;
    }
    public void setDiscardOption(String discardOption) {
        this.discardOption = discardOption;
    }
    public String getProducer() {
        return producer;
    }
    public void setProducer(String producer) {
        this.producer = producer;
    }
    public String getReagentType() {
        return reagentType;
    }
    public void setReagentType(String reagentType) {
        this.reagentType = reagentType;
    }
    /* ------------------------------------------------------------------------- */
    @Inject
    public SettingsBean (LoginBean loginBean) throws LoginExpiredExeption {
        this.loginBean = loginBean;
        init();
    }
    /* ------------------------------------------------------------------------- */
    private List<Producer> producers;
    public List getProducers() throws DbIOException{
        if(producers==null) {
            producers = (new ProducerDAO()).getAll(Producer.class);
        }
        return producers;
    }
    /* ------------------------------------------------------------------------- */
    public void addProducer() throws DbIOException{
        Producer p = (new ProducerDAO()).getByName(producer);
        if(p==null){
            (new ProducerDAO()).save(Producer.create(producer));
            producers = (new ProducerDAO()).getAll(Producer.class);
            Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        else{
            Msg.create(cz.getString("alreadySaved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        RequestContext.getCurrentInstance().update("settings");
    }
    /* ------------------------------------------------------------------------- */
    public void deleteProducer() throws DbIOException{
        Producer p = (new ProducerDAO()).getByName(producer);
        if(p!=null){
            (new ProducerDAO()).delete(p);
            producers = (new ProducerDAO()).getAll(Producer.class);
            Msg.create(cz.getString("deleted"),"", FacesMessage.SEVERITY_INFO,false);
        }
        else{
            Msg.create(cz.getString("alreadySaved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        RequestContext.getCurrentInstance().update("settings");
    }
    /* ------------------------------------------------------------------------- */
    public void deleteDiscardOption() throws DbIOException{
        DiscardOption p = (new DiscardOptionsDAO()).getByName(discardOption);
        if(p!=null){
            (new DiscardOptionsDAO()).delete(p);
            discardOptions = (new DiscardOptionsDAO()).getAll(DiscardOption.class);
            Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        else{
            Msg.create(cz.getString("alreadySaved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        RequestContext.getCurrentInstance().update("settings");
    }
    /* ------------------------------------------------------------------------- */
    public void addDiscardOption() throws DbIOException{
        DiscardOption p = (new DiscardOptionsDAO()).getByName(discardOption);
        if(p==null){
            (new DiscardOptionsDAO()).save(DiscardOption.create(discardOption));
            discardOptions = (new DiscardOptionsDAO()).getAll(DiscardOption.class);
            Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        else{
            Msg.create(cz.getString("alreadySaved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        RequestContext.getCurrentInstance().update("settings");
    }
    /* ------------------------------------------------------------------------- */
    private List<DiscardOption> discardOptions;
    public List getDiscardOptions() throws DbIOException{
        if(discardOptions==null) {
            discardOptions = (new DiscardOptionsDAO()).getAll(DiscardOption.class);
        }
        return discardOptions;
    }
    /* ------------------------------------------------------------------------- */
    private List<ReagentType> reagentTypes;
    public List getReagentTypes() throws DbIOException{
        if(reagentTypes==null) {
            reagentTypes = (new ReagentTypeDAO()).getAll(ReagentType.class);
        }
        return reagentTypes;
    }
    /* ------------------------------------------------------------------------- */
    public void addReagentType() throws DbIOException{
        ReagentType t = (new ReagentTypeDAO()).getByName(reagentType);
        if(t==null){
            (new ReagentTypeDAO()).save(ReagentType.create(reagentType));
            reagentTypes = (new ProducerDAO()).getAll(ReagentType.class);
            Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        else{
            Msg.create(cz.getString("alreadySaved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        RequestContext.getCurrentInstance().update("settings");
    }
    /* ------------------------------------------------------------------------- */
    public void deleteReagentType() throws DbIOException{
        ReagentType t = (new ReagentTypeDAO()).getByName(reagentType);
        if(t!=null){
            (new ReagentTypeDAO()).delete(ReagentType.create(reagentType));
            reagentTypes = (new ProducerDAO()).getAll(ReagentType.class);
            Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        else{
            Msg.create(cz.getString("alreadySaved"),"", FacesMessage.SEVERITY_INFO,false);
        }
        RequestContext.getCurrentInstance().update("settings");
    }

}