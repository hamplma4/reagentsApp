package reagents.app.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import reagents.data.kit.Kit;
import reagents.data.order.Catalog;
import reagents.data.order.Order;
import reagents.data.order.OrderItem;
import reagents.data.reagent.Reagent;
import reagents.data.reagent.ReagentType;
import reagents.data.settings.DiscardOption;
import reagents.data.settings.Producer;
import reagents.data.user.User;
import reagents.data.warnings.kits.WarnKitsExpi;
import reagents.data.warnings.kits.WarnKitsNumb;
import reagents.data.warnings.kits.WarnKitsSett;
 
public class HibernateUtil {
    private static SessionFactory sessionFactory;
     
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // loads configuration and mappings
            Configuration configuration = new Configuration().configure();
            configuration.addAnnotatedClass(User.class);
            configuration.addAnnotatedClass(Kit.class);
            configuration.addAnnotatedClass(Reagent.class);
            configuration.addAnnotatedClass(WarnKitsSett.class);
            configuration.addAnnotatedClass(WarnKitsExpi.class);
            configuration.addAnnotatedClass(WarnKitsNumb.class);
            configuration.addAnnotatedClass(Catalog.class);
            configuration.addAnnotatedClass(DiscardOption.class);
            configuration.addAnnotatedClass(Order.class);
            configuration.addAnnotatedClass(OrderItem.class);
            configuration.addAnnotatedClass(Producer.class);
            configuration.addAnnotatedClass(ReagentType.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
             
            // builds a session factory from the service registry
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);           
        }
         
        return sessionFactory;
    }
}