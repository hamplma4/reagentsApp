package reagents.app.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import reagents.app.ItemTemplate;
import reagents.app.exception.DbIOException;

public class DataView<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<T> all;
	private List<T> selected;
	private List<T> filtered;
	private List<T> edited;
	private T		select;
	private Date 	init;
	
	private ItemTemplate item;
	private String userName;

	public DataView(ItemTemplate item,String userName){
		this.item = item;
		this.userName = userName;
	}
	
	public List<T> getAll() {
		return all;
	}
	public void setAll(List<T> all){
		this.all = all;
	}
	public List<T> getSelected() {
		return selected;
	}
	public void setSelected(List<T> selected) {
		this.selected = selected;
	}
	public List<T> getFiltered() {
		return filtered;
	}
	public void setFiltered(List<T> filtered) {
		this.filtered = filtered;
	}
	public List<T> getEdited() {
		return edited;
	}
	public void setEdited(List<T> edited) {
		this.edited = edited;
	}
	
	public T getSelect() {
		return select;
	}

	public void setSelect(T select) {
		this.select = select;
	}
	/* ---------------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public void initAll() throws DbIOException{
		if (init == null || init.getTime() + 5000 < System.currentTimeMillis()) {
			init = new Date(System.currentTimeMillis());
			all = (List<T>)item.getService().getAll();
		}
	}
	/* ---------------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public void reload(){
		if(selected!=null && !selected.isEmpty()){
			edited = ((List<T>)item.getService().reload(selected));
			for(ItemTemplate i: (List<ItemTemplate>)edited){
				i.createBackUp();
			}
		}
		if(select!=null){
			List<T> list = new ArrayList<T>();
			list.add(select);
			list = (List<T>)item.getService().reload(list);
			select = list.get(0);
			((ItemTemplate)select).createBackUp();
		}
	}
	/* ---------------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public void backUp(){
		for(ItemTemplate i: (List<ItemTemplate>)edited){
			i.createBackUp();
		}
	}
	/* ---------------------------------------------------------------------- */
	public void lock(){
		if(edited != null && !edited.isEmpty()){
			item.getService().lock(edited,userName);
		}
		if(select!=null){
			List<T> list = new ArrayList<T>();
			list.add(select);
			item.getService().lock(list,userName);
		}
	}
	/* ---------------------------------------------------------------------- */
	public void save(){
		item.getService().save(edited);
	}
	/* ---------------------------------------------------------------------- */
	public void update(){
		item.getService().update(edited);
	}
	/* ---------------------------------------------------------------------- */
	public void unlock(){
		item.getService().unlock(userName);
	}
	/* ---------------------------------------------------------------------- */
	public void clear(){
		edited.clear();
	}
	/* ---------------------------------------------------------------------- */
	public void saveAll(){
		item.getService().save(all);
	}
	/* ---------------------------------------------------------------------- */
	@SuppressWarnings("unchecked")
	public void initCurrent() throws DbIOException{
		if (init == null || init.getTime() + 5000 < System.currentTimeMillis()) {
			init = new Date(System.currentTimeMillis());
			all = item.getService().initCurrent();
		}
	}
	/* ---------------------------------------------------------------------- */
	/* 	Check if items in edited list are Locked
		Return userName of user witch lock one of this items
		or "OK" if none */
	@SuppressWarnings("unchecked")
	public String isLocked(){
		for(ItemTemplate item: (List<ItemTemplate>)edited){
			if(item.isLocked(userName)){
				return item.getEditedBy();
			}
		}
		return "OK";
	}
  
}
