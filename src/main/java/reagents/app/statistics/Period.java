package reagents.app.statistics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Period {
    private Date start;
    private Date end;
    private int count;

    public Period(Date start, Date end){
        this.start = start;
        this.end = end;
        this.count = 0;
    }

    public void add(){
        this.count++;
    }

    public int getCount(){
        return this.count;
    }

    public Date getEnd(){ return end;}

    public void setEnd(Date end) { this.end = end; }

    public boolean belongs(Date date){
        return ((date.after(start) && date.before(end)) || date.getTime() == start.getTime() || date.getTime() == end.getTime());
    }

    public String getDate(){
        DateFormat df = new SimpleDateFormat("MM.yyyy");
        return df.format(start);
    }

    public static Date getLastDate(Date start){
        Calendar endDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        endDate.setTime(start);
        startDate.setTime(start);
        endDate.set(Calendar.DAY_OF_MONTH,startDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        endDate.set(Calendar.HOUR_OF_DAY,startDate.getActualMaximum(Calendar.HOUR_OF_DAY));
        endDate.set(Calendar.MINUTE,startDate.getActualMaximum(Calendar.MINUTE));
        endDate.set(Calendar.SECOND,startDate.getActualMaximum(Calendar.SECOND));
        endDate.set(Calendar.MILLISECOND,startDate.getActualMaximum(Calendar.MILLISECOND));
        return endDate.getTime();
    }

    public static Date getNextMonthFirstDate(Date end){
        Calendar endDate = Calendar.getInstance();
        endDate.setTime(end);
        endDate.set(Calendar.MONTH,endDate.get(Calendar.MONTH)+1);
        endDate.set(Calendar.DAY_OF_MONTH,endDate.getActualMinimum(Calendar.DAY_OF_MONTH));
        endDate.set(Calendar.HOUR_OF_DAY,endDate.getActualMinimum(Calendar.HOUR_OF_DAY));
        endDate.set(Calendar.MINUTE,endDate.getActualMinimum(Calendar.MINUTE));
        endDate.set(Calendar.SECOND,endDate.getActualMinimum(Calendar.SECOND));
        endDate.set(Calendar.MILLISECOND,endDate.getActualMinimum(Calendar.MILLISECOND));
        return endDate.getTime();
    }


}
