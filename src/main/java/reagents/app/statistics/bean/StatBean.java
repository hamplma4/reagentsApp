package reagents.app.statistics.bean;

import java.io.Serializable;
import java.util.*;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.*;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.kit.KitService;
import reagents.app.login.bean.LoginBean;
import reagents.app.statistics.Period;
import reagents.data.kit.Kit;

@SessionScoped
@Named
public class StatBean implements Serializable {

	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	private LoginBean loginBean;
	/* ------------------------------------------------------------------------- */
	private ResourceBundle cz;
	private String userName;
	/* ------------------------------------------------------------------------- */
	public void init() throws LoginExpiredExeption{
		if(userName==null) {
			userName = loginBean.getLoggedUserName();
		}
		this.createAreaModel();
	}
	/* ------------------------------------------------------------------------- */
	@Inject
	public StatBean (LoginBean loginBean) throws LoginExpiredExeption {
		this.loginBean = loginBean;
		this.cz = loginBean.getCz();
		this.dateMax = new Date();
		this.dateMin = new Date(1420070400000L);
		init();
	}
	/* ------------------------------------------------------------------------- */
	private String type;
	private Date dateMin;
	private Date dateMax;

	public Date getDateMax() {
		return dateMax;
	}

	public void setDateMax(Date dateMax) {
		this.dateMax = dateMax;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDateMin() {
		return dateMin;
	}

	public void setDateMin(Date dateMin) {
		this.dateMin = dateMin;
	}

	private BarChartModel chartModel;
	public BarChartModel getChartModel() {
		return chartModel;
	}

	public void createAreaModel() {



		chartModel = new BarChartModel();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<Kit> kits = (List<Kit>)(new KitService()).getByTypeStart(type);
		ChartSeries kitsChart = new ChartSeries();

		kitsChart.setLabel(cz.getString("numberOfExpiration"));

		List<Period> periods = new ArrayList<Period>();
		Date startDate = dateMin;
		Date endDate;
		while(startDate.getTime()<dateMax.getTime()){
			endDate = Period.getLastDate(startDate);
			periods.add(new Period(startDate,endDate));
			endDate = Period.getNextMonthFirstDate(endDate);
			startDate = endDate;
		}
		/* Oříznutí posledního intervalu podle zadaného data */
		periods.get(periods.size()-1).setEnd(dateMax);


		for(Period p: periods){
			for(Kit k:kits){
				if(p.belongs(k.getExpiration())) {
					p.add();
				}
			}
			kitsChart.set(p.getDate(),p.getCount());
		}

		chartModel.addSeries(kitsChart);

		chartModel.setLegendPosition("ne");
		chartModel.setStacked(true);
		chartModel.setShowPointLabels(true);

		Axis xAxis = new CategoryAxis(cz.getString("date"));
		xAxis.setTickAngle(-50);
		chartModel.getAxes().put(AxisType.X, xAxis);
		Axis yAxis = chartModel.getAxis(AxisType.Y);
		yAxis.setLabel(cz.getString("count"));
	}

	public void reload(){
		createAreaModel();
		RequestContext.getCurrentInstance().update("stat:chart");
	}
}