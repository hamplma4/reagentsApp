package reagents.app.reagent.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

import org.primefaces.context.RequestContext;
import reagents.app.Msg;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.app.reagent.*;
import reagents.app.view.DataView;
import reagents.data.reagent.*;
import reagents.data.settings.DiscardOption;
import reagents.data.settings.DiscardOptionsDAO;

@SessionScoped
@Named
public class ReagentBean implements Serializable {


	DiscardOptionsDAO 	discardOptionsDAO;
	ReagentTypeDAO		reagentTypeDAO;

	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	private DataView<Reagent> view;
	public DataView<Reagent> getView() {
		return view;
	}
	/* ------------------------------------------------------------------------- */
	/* Add Reagents Variables */
	private List<ReagentsAdd> add;
	public List<ReagentsAdd> getAdd() {
		return add;
	}
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	private LoginBean loginBean;
	/* ------------------------------------------------------------------------- */
	@Inject
	public ReagentBean (LoginBean loginBean) throws LoginExpiredExeption, DbIOException{
		this.loginBean = loginBean;
		this.cz = loginBean.getCz();
		init();
	}
	/* ------------------------------------------------------------------------- */
	private ResourceBundle cz;
	private String userName;
	private ReagentFunctions func;
	@PostConstruct  
	public void init() throws LoginExpiredExeption, DbIOException{
		if(userName==null){
			userName=loginBean.getLoggedUserName();
		}
		if(view == null){
			view = new DataView<>(new Reagent(),userName);
		}
		if(!showConsumed) {
			view.initCurrent();
		}
		else{
			view.initAll();
		}
		if(func==null){ 
			func = new ReagentFunctions(userName);
		}
	}
	/* ------------------------------------------------------------------------- */
	private boolean showConsumed=false;
	public boolean isShowConsumed() {
		return showConsumed;
	}
	/* ------------------------------------------------------------------------- */
	private String editable;
	public String getEditable() {
		return editable;
	}
	/* ------------------------------------------------------------------------- */
	public String editNext() {
		editable = "false";
		Msg.create(cz.getString("checkIt"),"", FacesMessage.SEVERITY_INFO, true);
		return "edit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String editPrev() {
		editable = "true";
		Msg.create(cz.getString("fillIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "edit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String edit() {
		if(view.getSelected().size()==0) {
			Msg.create(cz.getString("youMustSelectSomething"),"",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.reload();
		String result = func.checkIfLocked(view.getEdited());
		if (!result.equals("No")) {
			Msg.create(cz.getString("reagentIsLockedBy")+" "+result+".","",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.lock();
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "edit?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String openNext() {
		editable = "false";
		Msg.create(cz.getString("checkIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "open?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String openPrev() {
		editable = "true";
		Msg.create(cz.getString("fillIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "open?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String open() {
		if(view.getSelected().size()==0) {
			Msg.create(cz.getString("warning"),cz.getString("youMustSelectSomething"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.reload();
		String result = func.checkIfLocked(view.getEdited());
		if (!result.equals("No")) {
			Msg.create(cz.getString("reagentIsLockedBy")+" "+result+".","",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		if (func.checkIfOpened(view.getEdited())) {
			Msg.create(cz.getString("warning"),cz.getString("reagentAlreadyOpened"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.lock();
		for(Reagent r: view.getEdited()){
			r.setOpened(new Date());
		}
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "open?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String consumeNext() {
		editable = "false";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,false);
		return "consume?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String consumePrev() {
		editable = "true";
		Msg.create(cz.getString("checkIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "consume?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String consume() {
		if(view.getSelected().size()==0) {
			Msg.create(cz.getString("warning"),cz.getString("reagent.mustSelect"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.reload();
		String result = func.checkIfLocked(view.getEdited());
		if (result.equals("No")) {
			Msg.create(cz.getString("reagentIsLockedBy")+" "+result+".","",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		if (!func.checkIfConsumed(view.getEdited())) {
			Msg.create(cz.getString("reagentAlreadyConsumed"),"",FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		view.lock();
		for(Reagent r: view.getEdited()){
			r.setConsumed(new Date());
			r.setDiscardReason("Spotřebováno");
		}
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "consume?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String update() {
		view.update();
		Msg.create(cz.getString("saved"),"",FacesMessage.SEVERITY_INFO,true);
		return "reagent?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String add() throws DbIOException{
		add = new ArrayList<>();
		add.add(new ReagentsAdd(getReagentTypes().get(0).toString()));
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "add?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String addRow() throws DbIOException{
		add.add(new ReagentsAdd(getReagentTypes().get(0).toString()));
		Msg.create(cz.getString("rowAdded"),"",FacesMessage.SEVERITY_INFO,true);
		return "add?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String addSpecific() {
		if (add == null) {
			return "reagent?faces-redirect=true";
		}
		view.setEdited(new ArrayList<>());
		for (ReagentsAdd item : add) {
			for (int i = 0; i < item.getCount(); i++) {
				view.getEdited().add(new Reagent(item));
			}
		}
		editable = "true";
		Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
		return "addSpecific?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String addNext() {
		editable = "false";
		Msg.create(cz.getString("checkIt"),"",FacesMessage.SEVERITY_INFO,true);
		return "addSpecific?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String addPrev() {
		if(editable.equals("true")){
			Msg.create(cz.getString("info"),cz.getString("fillIt"),FacesMessage.SEVERITY_INFO,true);
			return "add?faces-redirect=true";
		}
		else{
			editable = "true";
			Msg.create(cz.getString("checkIt"),"",FacesMessage.SEVERITY_INFO,true);
			return "addSpecific?faces-redirect=true";
		}
	}
	/* ------------------------------------------------------------------------- */
	public String save() {
		add.clear();
		view.save();
		Msg.create(cz.getString("saved"),cz.getString(""),FacesMessage.SEVERITY_INFO,true);
		return "reagent?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */	
	public String cancel() {
		view.unlock();
		Msg.create(cz.getString("unlocked"),cz.getString(""),FacesMessage.SEVERITY_INFO,true);
		return "reagent?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public void showAll() throws DbIOException{
		if(!showConsumed){
			showConsumed = true;
			view.initAll();
			Msg.create(cz.getString("consumedIsShowed"),"",FacesMessage.SEVERITY_INFO,true);
		}
		else{
			showConsumed = false;
			view.initCurrent();
			Msg.create(cz.getString("consumedIsHidden"),"",FacesMessage.SEVERITY_INFO,true);
		}
		RequestContext.getCurrentInstance().update("reagent:table");
	}
	/* ------------------------------------------------------------------------- */
	private List<DiscardOption> discardOptions;
	@SuppressWarnings("unchecked")
	public List<DiscardOption> getDiscardOptions() throws DbIOException{
		if(discardOptions==null) {
			discardOptions = (List<DiscardOption>)discardOptionsDAO.getAll(DiscardOption.class);
		}
		return discardOptions;
	}
	/* ------------------------------------------------------------------------- */
	private List<ReagentType> reagentTypes;
	@SuppressWarnings("unchecked")
	public List<ReagentType> getReagentTypes() throws DbIOException{
		if(reagentTypes==null) {
			reagentTypes = (List<ReagentType>)reagentTypeDAO.getAll(ReagentType.class);
		}
		return reagentTypes;
	}
}
