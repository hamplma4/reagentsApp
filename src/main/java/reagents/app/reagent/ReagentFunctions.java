package reagents.app.reagent;

import java.util.List;

import reagents.data.reagent.Reagent;

public class ReagentFunctions {
	
	private String userName;
	
	public ReagentFunctions(String userName) {
		this.userName = userName;
	}

	public String checkIfLocked(List<Reagent> reagents) {
		for (Reagent r : reagents) {
			if(r.isLocked(userName)){
				return r.getEditedBy();
			}
		}
		return "No";
	}
	
	public boolean checkIfConsumed(List<Reagent> edited) {
		for(Reagent r: edited){
			if(r.getConsumed() != null){
				return true;
			}
		}
		return false;
	}

	public boolean checkIfOpened(List<Reagent> edited) {
		for(Reagent r: edited){
			if(r.getOpened() != null){
				return true;
			}
		}
		return false;
	}

}
