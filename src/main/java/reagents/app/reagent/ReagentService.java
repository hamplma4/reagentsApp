package reagents.app.reagent;

import java.util.List;

import org.hibernate.HibernateException;

import reagents.app.ServiceTemplate;
import reagents.data.reagent.Reagent;
import reagents.data.reagent.ReagentDAO;

public class ReagentService extends ServiceTemplate {

	public List<Reagent> getAll() throws HibernateException {
		return (new ReagentDAO()).getAll();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void update(List forUpdate) throws HibernateException {
		new ReagentDAO().update((List<Reagent>)forUpdate);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void save(List forSave) throws HibernateException {
		new ReagentDAO().update((List<Reagent>)forSave);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List reload(List selected) throws HibernateException {
		return (new ReagentDAO()).reload((List<Reagent>)selected);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void lock(List forLock, String user) throws HibernateException {
		new ReagentDAO().lock((List<Reagent>)forLock, user);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void unlock(String userName) throws HibernateException {
		(new ReagentDAO()).unlock(userName);
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List initCurrent() throws HibernateException {
		return (new ReagentDAO()).getNonConsumed();
	}
	
	
}
