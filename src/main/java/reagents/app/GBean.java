package reagents.app;

import java.io.Serializable;
import java.util.List;

import reagents.data.settings.DiscardOptionsDAO;
import reagents.data.settings.DiscardOption;


public class GBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	public List<DiscardOption> getDiscardOptions(){
		return (new DiscardOptionsDAO()).getAll();
	}
	
	public String redirect(String page) {
		return "/pages/" + page + "?faces-redirect=true";
	}
	
}
