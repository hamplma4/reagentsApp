package reagents.app.exception;

public class DbIOException extends Exception {

    private Exception hiddenEx;

    public DbIOException(Exception ex){
        hiddenEx = ex;
    }

    @Override
    public String toString(){
        return hiddenEx.toString();
    }
}
