package reagents.app.exception;

import java.io.IOException;
import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.hibernate.HibernateException;

public class CustomExceptionHandler extends ExceptionHandlerWrapper {
	  private ExceptionHandler wrapped;

	  public CustomExceptionHandler(ExceptionHandler wrapped) {
	    this.wrapped = wrapped;
	  }

	  @Override
	  public ExceptionHandler getWrapped() {
	    return wrapped;
	  }

	  @Override
	  public void handle() throws FacesException {
	    Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
	    while (i.hasNext()) {
	      ExceptionQueuedEvent event = i.next();
	      ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
	      Throwable t = context.getException();
	      FacesContext fc = context.getContext();
	      boolean exceptionHandled = handleException(t, fc);
	      if(exceptionHandled) i.remove(); // remove from queue
	    }
	    getWrapped().handle(); // let wrapped ExceptionHandler do the rest
	  }

	  private boolean handleException(Throwable t, FacesContext fc) {
	    if (t instanceof ViewExpiredException) {
	      try {
	        fc.getExternalContext().redirect("login.html");
	        return true;
	      } catch (IOException e) {
	        throw new FacesException(e);
	      }
	    }
	    if (t instanceof HibernateException) {
	    	fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Chyba Aplikace!", t.getMessage()));
	    	return true;
		}
		/*if(t instanceof JDBCException) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Chyba Aplikace!", t.getMessage()));
			return true;
		}*/
		  if(t instanceof Exception) {
			  fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Chyba Aplikace!", t.getMessage()));
			  return true;
		  }
          /*if(t instanceof ServletException) {
              fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Chyba Aplikace!", t.getMessage()));
              return true;
          }*/
		else return false;
	  }
}