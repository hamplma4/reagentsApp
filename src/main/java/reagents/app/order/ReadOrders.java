package reagents.app.order;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.primefaces.model.UploadedFile;

import reagents.data.order.Catalog;

public class ReadOrders {
	
	List<Catalog> catalog;
	
	@SuppressWarnings("static-access")
	public ReadOrders(UploadedFile file) throws IOException{
			InputStream f = file.getInputstream();

			HSSFWorkbook wb = new HSSFWorkbook(f);
			HSSFSheet ws = wb.getSheetAt(0);

			int rowNum = ws.getLastRowNum()+1;

			catalog = new ArrayList<Catalog>();
			for (int i=1; i<rowNum; i++){
				HSSFRow row = ws.getRow(i);
				String type = row.getCell(1,row.CREATE_NULL_AS_BLANK).getStringCellValue();

				if(!type.equals("")){
					int value = (int)row.getCell(2,row.CREATE_NULL_AS_BLANK).getNumericCellValue();
					String note = row.getCell(3,row.CREATE_NULL_AS_BLANK).getStringCellValue();
					String code = row.getCell(4,row.CREATE_NULL_AS_BLANK).getStringCellValue();
					Cell cell = row.getCell(5,row.CREATE_NULL_AS_BLANK);
					cell.setCellType(Cell.CELL_TYPE_STRING);
					int cost;
					try{
						cost = Integer.parseInt(cell.getStringCellValue());
					}
					catch(NumberFormatException Ex){
						cost = 0;
					}
					catalog.add(new Catalog(type,value,note,code,cost));
				}
			}
	}
	
	public List<Catalog> getData(){
		return catalog;
	}
}
