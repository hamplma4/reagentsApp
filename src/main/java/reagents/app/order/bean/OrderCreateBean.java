package reagents.app.order.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.app.order.OrderService;
import reagents.app.view.DataView;
import reagents.data.order.Catalog;
import reagents.data.order.Order;
import reagents.data.order.OrderItem;
 
@ManagedBean(name = "orderCreateBean", eager = true)
@SessionScoped
public class OrderCreateBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	private DataView<OrderItem> viewItems;
	public  DataView<OrderItem> getViewItems() {
		return viewItems;
	}
	/* ------------------------------------------------------------------------- */
	public DataView<Catalog> viewCatalog;
	public DataView<Catalog> getViewCatalog() {
		return viewCatalog;
	}
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	@ManagedProperty(value = "#{loginBean}")
	private LoginBean loginBean;
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	public LoginBean getLoginBean() {
		return loginBean;
	}
	/* ------------------------------------------------------------------------- */
	private Date newDate;
	private String newLabel;
	private ResourceBundle cz;
	private String userName;
	@PostConstruct
	public void init() throws DbIOException, LoginExpiredExeption {
		if(userName == null){
			userName = loginBean.getLoggedUserName();
		}
		if(viewCatalog == null){
			viewCatalog = new DataView<>(new Catalog(),userName);
			viewCatalog.initAll();
		}
		if(viewItems == null){
			viewItems = new DataView<>(new OrderItem(),userName);
		}		
		if(newDate == null){
			newDate = new Date();
		}
		if(newLabel == null){
			newLabel = "objednávka č.1";
		}
		if(cz == null){
			cz = ResourceBundle.getBundle("reagents.app.texts.text_cz");
		}
	}
	/* ------------------------------------------------------------------------- */
	public Date getNewDate() {
		return newDate;
	}

	public void setNewDate(Date newDate) {
		this.newDate = newDate;
	}

	public String getNewLabel() {
		return newLabel;
	}

	public void setNewLabel(String newLabel) {
		this.newLabel = newLabel;
	}
	/* ------------------------------------------------------ */
	public String addItems (){
		if(viewItems.getAll() == null){
			viewItems.setAll(new ArrayList<>());
		}
		for(Catalog c: viewCatalog.getSelected()){
			System.out.println("Pridavam vec"); 
			viewItems.getAll().add(new OrderItem(c));
		}
		return "createOrder?faces-redirect=true";
	}
	/* ------------------------------------------------------ */
	public String saveOrder(){ 
		if(viewItems.getAll() == null){
			createMsg(	cz.getString("error"),
						cz.getString("orderIsEmpty"), 
						FacesMessage.SEVERITY_WARN , false);
			return "";
		}
		if(newDate==null){
			createMsg(	cz.getString("error"),
						cz.getString("dateIsEmpty"), 
						FacesMessage.SEVERITY_WARN , false);
		return "";
		}
		int idOrder = (new OrderService()).getNextId();
		int costSum = 0;
		int count = 0;
		for(OrderItem item: viewItems.getAll()){
			if(item.getCount()==0){
				viewItems.getAll().remove(item);
			}
			item.setIdOrder(idOrder);
			costSum+=item.getCost()*item.getCount();
			count+=item.getCount()*item.getContain();
		}
		viewItems.saveAll();
		List<Order> orders = new ArrayList<>();
		orders.add(new Order(idOrder,newLabel,newDate,count,costSum));
		(new OrderService()).save(orders);
		return "createOrder?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	private void createMsg(String summary,String detail, Severity severity, boolean nextPage){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, summary, detail));
		if(nextPage){
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}	
}