package reagents.app.order.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import reagents.app.Msg;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.app.order.OrderFunctions;
import reagents.app.order.OrderItemService;
import reagents.app.order.OrderService;
import reagents.app.view.DataView;
import reagents.data.kit.Kit;
import reagents.data.order.Order;
import reagents.data.order.OrderItem;

@SessionScoped
@Named
public class OrderBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /* ------------------------------------------------------------------------- */
    private DataView<Order> view;
    public DataView<Order> getView() {
        return view;
    }
    /* ------------------------------------------------------------------------- */
    private DataView<OrderItem> viewItems;
    public DataView<OrderItem> getViewItems() {
        return viewItems;
    }
    /* ------------------------------------------------------------------------- */
    private DataView<Kit> viewKits;
    public DataView<Kit> getViewKits() {
        return viewKits;
    }
    /* ------------------------------------------------------------------------- */
    /* Variable LoginBean for access name of logged user. */
    private LoginBean loginBean;
    /* ------------------------------------------------------------------------- */
    private Date orderDate;
    private String orderLabel;
    private String editable;
    public Date getOrderDate() {
        return orderDate;
    }
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
    public String getOrderLabel() {
        return orderLabel;
    }
    public void setOrderLabel(String orderLabel) {
        this.orderLabel = orderLabel;
    }
    public String getEditable() {
        return editable;
    }
    /* ------------------------------------------------------------------------- */
    private Date initDate;
    private ResourceBundle cz;
    private String userName;
    private OrderFunctions func;
    public void init() throws LoginExpiredExeption, DbIOException{
        if (userName == null) {
            userName = loginBean.getLoggedUserName();
        }
        if (view == null) {
            view = new DataView<>(new Order(), userName);
        }
        if (initDate == null || initDate.getTime() + 5000 < System.currentTimeMillis()) {
            view.initCurrent();
            initDate = new Date(System.currentTimeMillis());
        }
        if(func == null){
            func = new OrderFunctions();
        }
    }
    /* ------------------------------------------------------------------------- */
    @Inject
    public OrderBean(LoginBean loginBean) throws LoginExpiredExeption, DbIOException {
        this.loginBean = loginBean;
        this.cz = loginBean.getCz();
        init();
    }
    /* ------------------------------------------------------------------------- */
    @SuppressWarnings("unchecked")
    public String edit() {
        if (view.getSelect() == null) {
            Msg.create(cz.getString("error"), cz.getString("youMustSelectSomething"),
                    FacesMessage.SEVERITY_WARN, true);
            return "";
        }
        view.reload();
        if (view.getSelect().isLocked(userName)) {
            Msg.create(cz.getString("error")
                    , cz.getString("isLockedBy") + " " + view.getEdited().get(0).getEditedBy() + "."
                    , FacesMessage.SEVERITY_WARN, true);
            return "";
        }
        view.lock();
        orderDate = view.getSelect().getDate();
        orderLabel = view.getSelect().getLabel();
        editable = "true";
        viewItems = new DataView<>(new OrderItem(), userName);
        viewItems.setEdited((new OrderItemService()).getByOrder(view.getSelect()));
        viewItems.backUp();
        return "edit?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String editNext() {
        editable = "false";
        Msg.create(cz.getString("adjustments"), cz.getString("checkIt"),
                FacesMessage.SEVERITY_INFO, true);
        return "edit?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String editPrev() {
        editable = "true";
        Msg.create(cz.getString("adjustments"), cz.getString("makeAdjustments"),
                FacesMessage.SEVERITY_INFO, true);
        return "edit?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String editUpdate() {
        viewItems.update();
        Order o = func.configure(view.getSelect().getId(),orderLabel,orderDate,viewItems.getEdited());
        o.unlock();
        (new OrderService()).update(o);
        view.setSelect(null);
        Msg.create(cz.getString("saved"), cz.getString("savedInfo"),
                FacesMessage.SEVERITY_INFO, true);
        return "orders?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String cancel() {
        Msg.create(cz.getString("unlocked"),"",FacesMessage.SEVERITY_INFO,true);
        view.unlock();
        return "orders?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    @SuppressWarnings("unchecked")
    public String export() {
        if (view.getSelect() == null) {
            Msg.create(cz.getString("error"), cz.getString("chooseOrder"),
                    FacesMessage.SEVERITY_WARN, true);
            return "";
        }
        viewItems = new DataView<>(new OrderItem(), userName);
        viewItems.setEdited((new OrderItemService()).getByOrder(view.getSelect()));
        return "export?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    @SuppressWarnings("unchecked")
    public String accept() {
        if (view.getSelect() == null) {
            Msg.create(cz.getString("error"), cz.getString("youMustSelectSomething"),
                    FacesMessage.SEVERITY_WARN, true);
            return null;
        }
        view.reload();
        if (view.getSelect().isLocked(userName)) {
            Msg.create(cz.getString("error")
                    , cz.getString("isLockedBy") + " " + view.getSelect().getEditedBy() + "."
                    , FacesMessage.SEVERITY_ERROR,true);
            return null;
        }
        view.lock();
        List<OrderItem> orderItems = (new OrderItemService()).getByOrder(view.getSelect());
        List<Kit> kits = new ArrayList<>();
        for (OrderItem item : orderItems) {
            for (int i = 0; i < item.getCount(); i++) {
                Kit k = new Kit(item);
                k.setArriveToLab(new Date());
                kits.add(k);
            }
        }
        viewKits = new DataView<>(new Kit(), userName);
        viewKits.setEdited(kits);
        editable = "true";
        Msg.create(cz.getString("fillBatchAndExpiration"), "", FacesMessage.SEVERITY_INFO, true);
        return "accept?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String acceptNext() {
        if(OrderFunctions.checkAccept(viewKits.getEdited())){
            editable = "false";
            Msg.create(cz.getString("checkIt"), "", FacesMessage.SEVERITY_INFO, true);
        }
        Msg.create(cz.getString("batchAndExpirationIsRequired"),"",FacesMessage.SEVERITY_INFO, true);
        return "accept?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String acceptPrev() {
        Msg.create(cz.getString("fillIt"),"",FacesMessage.SEVERITY_INFO, true);
        editable = "true";
        return "accept?faces-redirect=true";
    }

    /* ------------------------------------------------------------------------- */
    public String acceptIt() {
        viewKits.save();
        Order o = view.getSelect();
        o.setAcceptedDate(new Date());
        (new OrderService()).update(o);
        Msg.create(cz.getString("saved"),"",FacesMessage.SEVERITY_INFO, true);
        return "orders?faces-redirect=true";
    }
    /* ------------------------------------------------------------------------- */
    public String delete() {
        if (view.getSelect() == null) {
            Msg.create(cz.getString("error"), cz.getString("chooseOrder"),
                    FacesMessage.SEVERITY_WARN, true);
            return "";
        }
        RequestContext.getCurrentInstance().execute("PF('dlg2').show();");
        return "";
    }
}