package reagents.app.order.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;

import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.app.order.CatalogService;
import reagents.app.order.ReadOrders;
import reagents.app.view.DataView;
import reagents.data.order.Catalog;
 
@ManagedBean(name = "catalogBean", eager = true)
@SessionScoped
public class CatalogBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	private DataView<Catalog> view;
	public DataView<Catalog> getView() {
		return view;
	}
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	@ManagedProperty(value = "#{loginBean}")
	private LoginBean loginBean;
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	public LoginBean getLoginBean() {
		return loginBean;
	}
	/* ------------------------------------------------------------------------- */
	private Date initDate;
	private String userName;
	@PostConstruct
	public void init() throws DbIOException, LoginExpiredExeption {
		if(userName==null){
			userName = loginBean.getLoggedUserName();
		}
		if(view == null){
			view = new DataView<Catalog>(new Catalog(),userName);
		}
		if (initDate == null || initDate.getTime() + 5000 < System.currentTimeMillis()) {
			view.initAll();
			initDate = new Date(System.currentTimeMillis());
		}		
	}
	/* ------------------------------------------------------ */
    private UploadedFile file;
    public UploadedFile getFile() {
        return file;
    }
    public void setFile(UploadedFile file) {
        this.file = file;
    }
    /* ------------------------------------------------------ */  
	public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            try {
				view.setAll((new ReadOrders(file)).getData());
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
	/* ------------------------------------------------------ */
	public void delete(Catalog item){
		view.getAll().remove(item);
	}
	/* ------------------------------------------------------ */
	public String save(){
		(new CatalogService()).save(view.getAll());
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uloženo", "Catalog uložen do DB"));
		return "";
	}
	/* ------------------------------------------------------ */
	public String deleteAllCatalog(){
		(new CatalogService()).deleteAll();
		view.getAll().clear();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Smazáno", "Catalog smazán z DB"));
		return "";
	}
}