package reagents.app.order;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import reagents.app.ServiceTemplate;
import reagents.data.order.CatalogDAO;
import reagents.data.order.Order;
import reagents.data.order.OrderDAO;

public class OrderService extends ServiceTemplate {

	@SuppressWarnings("rawtypes")
	@Override
	public List getAll() throws HibernateException {
		return (new OrderDAO()).getAll();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List reload(List selected) throws HibernateException {
		return (new OrderDAO()).reload(selected);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void lock(List forLock, String user) throws HibernateException {
		(new OrderDAO()).lock(forLock,user);
		
	}

	@Override
	public void unlock(String userName) throws HibernateException {
		(new OrderDAO()).unlock(userName);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void update(List forUpdate) throws HibernateException {
		(new OrderDAO()).update(forUpdate);
		
	}

	public void update(Order order) throws HibernateException {
		List<Order> list = new ArrayList<Order>();
		list.add(order);
		(new OrderDAO()).update(list);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void save(List forSave) throws HibernateException {
		(new OrderDAO()).save(forSave);
		
	}

	public void deleteAll() {
		(new CatalogDAO()).deleteAll();
	}
	
	public int getNextId(){
		return (new OrderDAO()).getNextId();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List initCurrent() throws HibernateException {
		return (new OrderDAO()).getNonAceppted();
	}
	
	
}
