package reagents.app.order;

import java.util.List;

import org.hibernate.HibernateException;

import reagents.app.ServiceTemplate;
import reagents.data.order.CatalogDAO;
import reagents.data.order.OrderDAO;

public class CatalogService extends ServiceTemplate {

	@SuppressWarnings("rawtypes")
	@Override
	public List getAll() throws HibernateException {
		return (new CatalogDAO()).getAll();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List reload(List selected) throws HibernateException {
		return (new CatalogDAO()).reload(selected);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void lock(List forLock, String user) throws HibernateException {
		(new OrderDAO()).lock(forLock,user);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void update(List forUpdate) throws HibernateException {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void save(List forSave) throws HibernateException {
		(new CatalogDAO()).save(forSave);
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void unlock(String userName) throws HibernateException {
		(new CatalogDAO()).unlock(userName);
	}

	public void deleteAll() {
		(new CatalogDAO()).deleteAll();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List initCurrent() throws HibernateException {
		return null;
	}

	
}
