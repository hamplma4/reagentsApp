package reagents.app.order;

import reagents.data.kit.Kit;
import reagents.data.order.Order;
import reagents.data.order.OrderItem;

import java.util.Date;
import java.util.List;

public class OrderFunctions {

    public Order configure(int id, String label, Date date, List<OrderItem> items){
        Order o = new Order();
        int costSum = 0;
        int countSum = 0;
        for (OrderItem i : items) {
            costSum += i.getCost() * i.getCount();
            countSum += i.getCount() * i.getContain();
        }
        o.setSumCost(costSum);
        o.setCount(countSum);
        o.setDate(date);
        o.setLabel(label);
        o.setId(id);
        return o;
    }


    public static boolean checkAccept(List<Kit> edited) {
        for(Kit item: edited){
            if(item.getBatch() == null || item.getBatch().equals("")) return false;
            if(item.getExpiration() == null) return false;
        }
        return true;
    }
}
