package reagents.app.order;

import java.util.List;

import org.hibernate.HibernateException;

import reagents.app.ServiceTemplate;
import reagents.data.order.CatalogDAO;
import reagents.data.order.Order;
import reagents.data.order.OrderDAO;
import reagents.data.order.OrderItemDAO;

public class OrderItemService extends ServiceTemplate {

	@SuppressWarnings("rawtypes")
	@Override
	public List getAll() throws HibernateException {
		return null; //(new OrderDAO()).getAll();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List reload(List selected) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void lock(List forLock, String user) throws HibernateException {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void update(List forUpdate) throws HibernateException {
		(new OrderItemDAO()).update(forUpdate);	
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void save(List forSave) throws HibernateException {
		(new OrderItemDAO()).save(forSave);
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void unlock(String userName) throws HibernateException {
		/* Cannot unlock this item */
	}

	@Override
	public List initCurrent() throws HibernateException {
		return null;
	}

	public void deleteAll() {
		(new CatalogDAO()).deleteAll();
	}
	
	public int getNextId(){
		return (new OrderDAO()).getNextId();
	}
	
	@SuppressWarnings("rawtypes")
	public List getByOrder(Order o){
		return (new OrderItemDAO()).getByOrder(o);
	}
	
}
