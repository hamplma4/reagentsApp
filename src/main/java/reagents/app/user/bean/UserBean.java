package reagents.app.user.bean;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;


import org.primefaces.context.RequestContext;
import reagents.app.Msg;
import reagents.app.Stage;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;
import reagents.app.user.UserFunctions;
import reagents.app.view.DataView;
import reagents.data.user.User;


@SessionScoped
@Named
public class UserBean implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/* ------------------------------------------------------------------------- */
	private DataView<User> view;
	public DataView<User> getView() {
		return view;
	}
	/* ------------------------------------------------------------------------- */
	/* Variable LoginBean for access name of logged user. */
	private LoginBean loginBean;
	/* ------------------------------------------------------------------------- */
	private ResourceBundle cz;
	private String userName;
	private UserFunctions func;
	private Stage stage;
	public Stage getStage() {
		return stage;
	}
	/* ------------------------------------------------------------------------- */
	public void init() throws DbIOException, LoginExpiredExeption {
		if(userName==null){
			userName = loginBean.getLoggedUserName();
		}
		if(view == null){
			view = new DataView<>(new User(),userName);
		}
		view.initAll();
		if(func==null) func = new UserFunctions();
		if(cz == null) cz = loginBean.getCz();
		if(stage == null) stage = Stage.ALL;
	}
	/* ------------------------------------------------------------------------- */
	@Inject
	public UserBean (LoginBean loginBean) throws DbIOException, LoginExpiredExeption {
		this.loginBean = loginBean;
		init();
	}
	/* ------------------------------------------------------------------------- */
	/* Variables */
	private User 	user;
	private String 	passwd;
	private String  disabled;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd1) {
		this.passwd = passwd1;
	}
	public String getDisabled() {
		return disabled;
	}

	/* Methods for edit user */
	/* ------------------------------------------------------------------------- */
	public String edit(){
		if(view.getSelect() == null){
			Msg.create(cz.getString("warning"),cz.getString("youMustSelectSomething"),FacesMessage.SEVERITY_WARN,false);
			return "";
		}
		user = view.getSelect();
		user.reload();
		if(user.isLocked(userName)){
			Msg.create(cz.getString("error"),cz.getString("isLockedBy")+" "+user.getEditedBy() +".",FacesMessage.SEVERITY_ERROR,false);
			return "";
		}
		user.lock(userName);
		user.createBackUp();
		passwd = "";
		disabled = "false";
		stage = Stage.EDIT;
		Msg.create(cz.getString("fillIt"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users");
		return "users?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String editNext(){
		// Check if email is no already used
		if(!user.getEmail().equals(user.getBackUp().getEmail())
				&& !func.checkUniqueMail(user)){
			Msg.create(cz.getString("mailIsUsed"),"", FacesMessage.SEVERITY_ERROR,true);
			RequestContext.getCurrentInstance().update("users:edit:panel");
			return "users?faces-redirect=true";
		}
		// Check if email is no already used
		if(!user.getUserName().equals(user.getBackUp().getUserName())
				&& !func.checkUniqueUserName(user)){
			Msg.create(cz.getString("userNameIsUsed"),"", FacesMessage.SEVERITY_ERROR,true);
			RequestContext.getCurrentInstance().update("users:edit:panel");
			return "users?faces-redirect=true";
		}
		if(passwd.equals("")) passwd = cz.getString("passwdIsNotFilled");
		else{
			user.setPasswd(UserFunctions.makeHash(passwd));
			passwd = cz.getString("passwdIsFilled");
		}
		disabled = "true";
		Msg.create(cz.getString("checkIt"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users:edit:panel");
		return "users?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String editPrev(){
		System.out.println("editPrev");
		disabled = "false";
		passwd = "";
		Msg.create(cz.getString("fillIt"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users:edit:panel");
		return "users?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String add(){
		user = new User();
		passwd = "";
		stage = Stage.ADD;
		disabled = "false";
		Msg.create(cz.getString("fillIt"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users");
		return "users?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String addNext(){
		if(!func.checkUniqueMail(user)){
			Msg.create(cz.getString("mailIsUsed"),"", FacesMessage.SEVERITY_ERROR,false);
			RequestContext.getCurrentInstance().update("users:add:panel");
			return "users?faces-redirect=true";
		}
		if(!func.checkUniqueUserName(user)){
			Msg.create(cz.getString("userNameIsUsed"),"", FacesMessage.SEVERITY_ERROR,false);
			RequestContext.getCurrentInstance().update("users:add:panel");
			return "users?faces-redirect=true";
		}
		user.setPasswd(UserFunctions.makeHash(passwd));
		passwd = "Heslo vyplněno";
		disabled = "true";
		Msg.create(cz.getString("checkIt"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users:add:panel");
		return "users?faces-redirect=true";
	}
	/* ------------------------------------------------------------------------- */
	public String addPrev(){
		disabled = "false";
		Msg.create(cz.getString("fillIt"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users:add:panel");
		return "users.xhtml";
	}
	/* ------------------------------------------------------------------------- */
	public String saveUser() throws DbIOException{
		if(stage == Stage.ADD) user.save();
		if(stage == Stage.EDIT) user.update();
		stage = Stage.ALL;
		view.initAll();
		Msg.create(cz.getString("saved"),"", FacesMessage.SEVERITY_INFO,false);
		RequestContext.getCurrentInstance().update("users");
		return "users?faces-redirect=true";
	}
	/* ---------------------------------------------------- */
	public String getRoleName (String role){
		int r = Integer.parseInt(role);
		if(r==5) return "Administrátor";
		if(r==3) return "Čtení a Zápis";
		if(r==1) return "Pouze Čtení";
		return "Účet zablokován";
	}	
	/* ---------------------------------------------------- */
	public String redirect(String page){
		return "/pages/users/"+page+"?faces-redirect=true";
	}
	/* ---------------------------------------------------- */
	public String cancel(){
		stage = Stage.ALL;
		user.unlock();
		return "users?faces-redirect=true";
	}
}
