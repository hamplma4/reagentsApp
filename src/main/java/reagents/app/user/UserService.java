package reagents.app.user;

import org.hibernate.HibernateException;
import reagents.app.ServiceTemplate;
import reagents.app.exception.DbIOException;
import reagents.data.user.User;
import reagents.data.user.UserDAO;

import java.util.List;

public class UserService extends ServiceTemplate {

	/* --------------------------------------------------------------- */
	public static User getByUserName(String userName){
		return (new UserDAO()).getByUserName(userName);
	}
	/* --------------------------------------------------------------- */
	public static User getByMail(String mail){
		return (new UserDAO()).getByMail(mail);
	}
	/* --------------------------------------------------------------- */
	public static void update(User user){
		(new UserDAO()).update(user);
	}
	/* --------------------------------------------------------------- */
	public static void save(User user){
		(new UserDAO()).save(user);
	}
	/* --------------------------------------------------------------- */
	@Override
	public List getAll() throws DbIOException {
		return (new UserDAO()).getAll();
	}
	/* --------------------------------------------------------------- */
	@Override
	public List reload(List selected) throws HibernateException {
		return null;
	}
	/* --------------------------------------------------------------- */
	@Override
	public void lock(List forLock, String userName) throws HibernateException {
		(new UserDAO()).lock((User)forLock.get(0),userName);
	}
	/* --------------------------------------------------------------- */
	public void lock(User u, String userName) throws HibernateException {
		(new UserDAO()).lock(u,userName);
	}
	/* --------------------------------------------------------------- */
	@Override
	public void update(List forUpdate) throws HibernateException {
		(new UserDAO()).update((User)forUpdate.get(0));
	}
	/* --------------------------------------------------------------- */
	@Override
	public void save(List forSave) throws HibernateException {
		(new UserDAO()).save((User)forSave.get(0));
	}
	/* --------------------------------------------------------------- */
	@Override
	public void unlock(String userName) throws HibernateException {
		(new UserDAO()).unlock(userName);
	}
	/* --------------------------------------------------------------- */
	@Override
	public List initCurrent() throws DbIOException {
		return (new UserDAO()).getAll();
	}
}
