package reagents.app.user;

import reagents.data.user.User;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserFunctions {

	/* ----------------------------------------------------------------------------------- */
	public static boolean checkUniqueUserName(User u){
		return (new UserService()).getByUserName(u.getUserName()) == null;
	}
	/* ----------------------------------------------------------------------------------- */
	public static boolean checkUniqueMail(User u){
		return (new UserService()).getByMail(u.getEmail()) == null;
	}
	/* ----------------------------------------------------------------------------------- */
	public static String makeHash(String password){
		MessageDigest m;
		String p = password;
		try {
			m = MessageDigest.getInstance("MD5");
			m.update(password.getBytes(), 0, password.length());
			p = new BigInteger(1, m.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return p;
	}
	/* ----------------------------------------------------------------------------------- */
}
