package reagents.app.warnings.service;

import java.io.Serializable;

import reagents.data.warnings.kits.WarnKitsSett;
import reagents.data.warnings.kits.WarnKitsSettDAOImpl;

public class WarnKitsSettService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static WarnKitsSett getByType(String type){
		WarnKitsSettDAOImpl dao = new WarnKitsSettDAOImpl();
		return dao.getByType(type);
	}
}
