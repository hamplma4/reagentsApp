package reagents.app.warnings.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import reagents.data.warnings.kits.WarnKitsNumb;
import reagents.data.warnings.kits.WarnKitsNumbDAO;
import reagents.data.warnings.kits.WarnKitsNumbDAOImpl;

public class WarnKitsNumbService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static WarnKitsNumb getByType(String type) throws HibernateException{
		WarnKitsNumbDAO dao = new WarnKitsNumbDAOImpl();
		return dao.getByType(type);
	}

	public static void updateOrSave(WarnKitsNumb wkn) throws HibernateException{
		WarnKitsNumbDAO dao = new WarnKitsNumbDAOImpl();
		dao.save(wkn);
	}

	public static void delete(WarnKitsNumb wkn) throws HibernateException{
		WarnKitsNumbDAO dao = new WarnKitsNumbDAOImpl();
		dao.delete(wkn);
	}

	public static List<WarnKitsNumb> getAllActived() throws HibernateException{
		WarnKitsNumbDAO dao = new WarnKitsNumbDAOImpl();
		return dao.getAllActived();
	}

}
