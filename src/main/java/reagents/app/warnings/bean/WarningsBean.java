package reagents.app.warnings.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import reagents.app.view.DataView;
import reagents.app.warnings.WarningsCheck;
import reagents.data.kit.Kit;
import reagents.data.warnings.kits.WarnKitsNumb;

@ManagedBean(name = "warningsBean", eager = true)
@SessionScoped
public class WarningsBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DataView<Kit> viewKitsExpi;
	private DataView<WarnKitsNumb> viewKitsNumb;
		
	//private Date initDate;

	@PostConstruct
	public void init() {
		/*
		if(viewKitsExpi == null){
			viewKitsExpi = new DataView<Kit>();
			
		}
		if(viewKitsNumb == null){
			viewKitsNumb = new DataView<WarnKitsNumb>();
		}
		if (initDate == null || initDate.getTime() + 5000 < System.currentTimeMillis()) {
			try {
				viewKitsExpi.setAll(KitService.getByIds(WarnKitsExpiService.getAllActived()));
				viewKitsNumb.setAll(WarnKitsNumbService.getAllActived());
			} catch (HibernateException hEx) {
				hEx.printStackTrace();
			}
			initDate = new Date(System.currentTimeMillis());
		}*/
	}
	/* ---------------------------------------------------- */
	public DataView<Kit> getViewKitsExpi() {
		return viewKitsExpi;
	}
	/* ---------------------------------------------------- */
	public DataView<WarnKitsNumb> getViewKitsNumb() {
		return viewKitsNumb;
	}
	/* ---------------------------------------------------- */
	public String tryExecute(){
		System.out.println("Kontroluji");
		WarningsCheck job = new WarningsCheck();
		job.checkExpirationKits();
		return "warnings";
	}
}
