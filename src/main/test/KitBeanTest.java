import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.omg.CORBA.DATA_CONVERSION;
import reagents.app.exception.DbIOException;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.kit.bean.KitBean;
import reagents.app.login.bean.LoginBean;

@RunWith(JUnit4.class)
public class KitBeanTest extends TestCase {

    @Test
    public void nonLogedUserTest(){
        try{
            KitBean bean = new KitBean(new LoginBean());
            assertTrue(false);
        }
        catch (LoginExpiredExeption ex){
            assertTrue(true);
        }
        catch (DbIOException ex){
            assertTrue(false);
        }

    }

    @Test
    public void LoadKitsTest(){
        LoginBean loginBean = new LoginBean();
        loginBean.setUserName("hamplma4");
        loginBean.setPassword("admin");
        String redirect = loginBean.userLogin();
        try {
            KitBean bean = new KitBean(loginBean);
        } catch (Exception ex) {
            assertTrue(false);
        }
        assertTrue(true);
    }
}