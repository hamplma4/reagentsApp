import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import reagents.app.exception.LoginExpiredExeption;
import reagents.app.login.bean.LoginBean;

@RunWith(JUnit4.class)
public class LoginBeanTest extends TestCase {

    @Test
    public void loginTest(){
        LoginBean bean = new LoginBean();
        bean.setUserName("hamplma4");
        bean.setPassword("admin");
        String redirect = bean.userLogin();
        System.out.println(redirect);
        assertEquals(redirect,"/pages/kits/kit?faces-redirect=true");
    }

    @Test
    public void nonLoggedUserTest(){
        LoginBean bean = new LoginBean();
        try {
            String userName = bean.getLoggedUserName();
            assertTrue(false);
        } catch (LoginExpiredExeption loginExpiredExeption) {
            assertTrue(true);
        }
    }

    @Test
    public void logoutTest(){
        LoginBean bean = new LoginBean();
        bean.setUserName("hamplma4");
        bean.setPassword("admin");
        String redirect = bean.userLogin();
        bean.logout();
        try {
            String userName = bean.getLoggedUserName();
            assertTrue(false);
        } catch (LoginExpiredExeption loginExpiredExeption) {
            assertTrue(true);
        }
    }

    @Test
    public void securityCheckAdminTest(){
        LoginBean bean = new LoginBean();
        bean.setUserName("hamplma4");
        bean.setPassword("admin");
        String redirect = bean.userLogin();
        assertTrue(bean.securityCheck("5"));
    }





}